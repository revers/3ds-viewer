/* 
 * File:   ColorButton.cpp
 * Author: Revers
 * 
 * Created on 8 listopad 2010, 01:32
 */

#include <QtGui>
#include "ColorButton.h"

ColorButton::ColorButton(QColor color, QWidget* parent) : QPushButton(parent),
color(color) {

}

ColorButton::~ColorButton() {
}

void ColorButton::paintEvent(QPaintEvent* pe) {

    QPushButton::paintEvent(pe);
    QPainter painter(this);
    QPen pen(color.toRgb());

    int y = height() / 2;
    pen.setWidth(9);
    painter.setPen(pen);
    int margin = 14;
    painter.drawLine(margin, y, width() - margin, y);


}

