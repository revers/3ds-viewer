/*
 * File:   main.cpp
 * Author: Revers
 *
 * Created on 3 listopad 2010, 22:04
 */

#include <QtGui/QApplication>

#include "MainWindow.h"

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    MainWindow window;
    window.show();

    return app.exec();
}
