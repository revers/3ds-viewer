#ifndef TREEMODEL_H
#define TREEMODEL_H

#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>

#include <vector>
#include "My3dsLoader.h"
#include "treeitem.h"

#define TREE_COLUMN_NAME ""
#define CHECK_COLUMN 0

//class TreeItem;

class TreeModel : public QAbstractItemModel {

    Q_OBJECT
private:
    enum SubItemsState {
        ALL_CHECKED, ALL_UNCHECKED, ALL_MIXED
    };
   // My3dsLoader* loader;
    Array<MeshInfoBlock*>* meshGraph;
    Array<TreeItem*>* treeItemList;
    //    std::vector<TreeItem*> selectedItems;
    //
    //    TreeItem* lastSelectedItem;



    Array<TreeItem*>* loadNodes(Array<MeshInfoBlock*>* meshInfoGraph, TreeItem* parent);

    void setProperCheckState(const QModelIndex &index);
    void setProperCheckState(const QModelIndex &index, Qt::CheckState state);
    SubItemsState getSubItemsState(const QModelIndex &index, bool checkCurrent = false);
public:
    //    TreeModel(const QStringList &headers, const QString &data,
    //            QObject *parent = 0);

    TreeModel(Array<MeshInfoBlock*>* meshGraph, QObject* parent = NULL);

    TreeModel(QObject* parent = NULL) : QAbstractItemModel(parent) {
        rootItem = new TreeItem(TREE_COLUMN_NAME);
        treeItemList = NULL;
      //  loader = NULL;
        meshGraph = NULL;
    }

    Array<TreeItem*>* getTreeItemList() {
        return treeItemList;
    }

    ~TreeModel();

public:
    void loadRows(const char* title, Array<MeshInfoBlock*>* meshGraph);

    void removeAllRows() {
        int rows = rootItem->childCount();
       // cout << "rootItem->childCount = " << rows << endl;
        if (rows > 0)
            removeRows(0, rows);
        if (treeItemList != NULL) {
            delete treeItemList;
        }
    }

public:

    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
            int role = Qt::DisplayRole) const;

    QModelIndex index(int row, int column,
            const QModelIndex &parent = QModelIndex()) const;
    QModelIndex parent(const QModelIndex &index) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    int columnCount(const QModelIndex &parent = QModelIndex()) const;

    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,
            int role = Qt::EditRole);
    bool setHeaderData(int section, Qt::Orientation orientation,
            const QVariant &value, int role = Qt::EditRole);

    bool insertColumns(int position, int columns,
            const QModelIndex &parent = QModelIndex());
    bool removeColumns(int position, int columns,
            const QModelIndex &parent = QModelIndex());
    bool insertRows(int position, int rows,
            const QModelIndex &parent = QModelIndex());
    bool removeRows(int position, int rows,
            const QModelIndex &parent = QModelIndex());

private:
    void setupModelData(const QStringList &lines, TreeItem *parent);
    TreeItem *getItem(const QModelIndex &index) const;

    TreeItem *rootItem;
};

#endif
