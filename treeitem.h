#ifndef TREEITEM_H
#define TREEITEM_H

#include <QList>
#include <QVariant>
#include <QVector>
#include <QTreeWidgetItem>
#include "MeshInfoBlock.h"
#include <iostream>

using namespace std;

class TreeItem : public QTreeWidgetItem {
public:

    //  QVariant value;

    TreeItem(MeshInfoBlock* meshInfo, TreeItem *parent = 0) :
    meshInfo(meshInfo), parentItem(parent) {
        checked = true;
        this->itemData = *(new QVector<QVariant >);
        this->itemData << QVariant(QString(meshInfo->getFullName().c_str()));
    }

    TreeItem(const char* headerName, TreeItem *parent = 0) :
    meshInfo(meshInfo), parentItem(parent) {
        checked = true;
        this->itemData = *(new QVector<QVariant >);
        this->itemData << QVariant(QString(headerName));
    }

    //TreeItem(const QVector<QVariant> &data, TreeItem *parent = 0);
    ~TreeItem();

    void addChild(TreeItem* child) {
        //int position = childItems.count();
        childItems.append(child);
    }


    TreeItem *child(int number);
    int childCount() const;
    int columnCount() const;
    QVariant data(int column) const;
    bool insertChildren(int position, int count, int columns);
    bool insertColumns(int position, int columns);
    TreeItem *parent();
    bool removeChildren(int position, int count);
    bool removeColumns(int position, int columns);
    int childNumber() const;
    bool setData(int column, const QVariant &value);

    MeshInfoBlock* getMeshInfo() const {
        return meshInfo;
    }

    void setMeshInfo(MeshInfoBlock* meshInfo) {
        this->meshInfo = meshInfo;
    }

    bool isChecked() const {
        return checked;
    }

    void setChecked(bool checked) {
        this->checked = checked;
        
    }
private:
    void deleteAll(QList<TreeItem*>& childItems);
    MeshInfoBlock* meshInfo;
    QList<TreeItem*> childItems;
    QVector<QVariant> itemData;
    TreeItem *parentItem;
    bool checked;
};

#endif
