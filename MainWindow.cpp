/* 
 * File:   MainWindow.cpp
 * Author: Revers
 * 
 * Created on 3 listopad 2010, 22:05
 */

#include <QtGui>
#include <iostream>

#include "BorderLayout.h"
#include "MainWindow.h"
#include "GLWidget.h"

using namespace std;

MainWindow::MainWindow() {
    canChangeTrans = true;
    canChangeRot = true;
    canChangeScale = true;
    canChangePosition = true;
    glWidget = NULL;
    loader = NULL;
    QTextCodec::setCodecForTr(QTextCodec::codecForName("UTF-8"));

    createMenu();
    initComponents();
    connectSlots();

    move(100, 100);
    setWindowTitle(tr("My 3DS Viewer 0.1"));
    setMinimumSize(600, 600);
    resize(1024, 768);

    enlightObjectsAction->setChecked(glWidget->isEnlightSelection());
    drawOnlySelectedAction->setChecked(glWidget->isDrawOnlySelected());
    showGridAction->setChecked(glWidget->isWireMode());
    drawCoordSystemAction->setChecked(glWidget->isDrawCoordSystem());
    drawLocalCoordsAction->setChecked(glWidget->isDrawLocalCoordSystem());
    drawObjectFrameAction->setChecked(glWidget->isDrawObjectFrame());

    connect(glWidget, SIGNAL(refreshHierarchy()),
            meshTree, SLOT(refreshHierarchy()));
}

MainWindow::~MainWindow() {
}

void MainWindow::initComponents() {
    mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);

    glWidget = new GLWidget(mainWidget);
    meshTreeScroll = new QScrollArea(this);
    meshTree = new QMeshInfoTree(meshTreeScroll);
    BorderLayout* meshTreeLayout = new BorderLayout;
    meshTreeLayout->addWidget(meshTree, BorderLayout::Center);
    meshTreeScroll->setLayout(meshTreeLayout);
   // meshTreeScroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
   // meshTreeScroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    meshTreeScroll->setFixedWidth(250);

    toolTabWidget = new QTabWidget(this);
    modelTab = new QWidget(toolTabWidget);
    lightTab = new QWidget(toolTabWidget);
    toolTabWidget->addTab(modelTab, tr("Model"));
    toolTabWidget->addTab(lightTab, tr("Światło"));

    BorderLayout* layout = new BorderLayout();
    layout->addWidget(glWidget, BorderLayout::Center);
    layout->addWidget(meshTreeScroll, BorderLayout::East);
    layout->addWidget(toolTabWidget, BorderLayout::South);

    mainWidget->setLayout(layout);



    transPanel = new QGroupBox(tr("Translacja"), modelTab);

    transAllCB = new QCheckBox(tr("Wszytkie jednakowe wartości"), transPanel);
    transXL = new QLabel(tr("x:"), transPanel);
    transYL = new QLabel(tr("y:"), transPanel);
    transZL = new QLabel(tr("z:"), transPanel);
    transXSpin = new QDoubleSpinBox(transPanel);
    transYSpin = new QDoubleSpinBox(transPanel);
    transZSpin = new QDoubleSpinBox(transPanel);

    QPalette palet = transXL->palette();
    palet.setColor(QPalette::Background, QColor(0, 0, 0, 0));
    transXL->setPalette(palet);

    QVBoxLayout *transPanelLayout = new QVBoxLayout;
    transPanelLayout->addWidget(transAllCB);
    QGridLayout *transPanelLayout2 = new QGridLayout;
    transPanelLayout2->addWidget(transXL, 0, 0);
    transPanelLayout2->addWidget(transYL, 1, 0);
    transPanelLayout2->addWidget(transZL, 2, 0);
    transPanelLayout2->addWidget(transXSpin, 0, 1, Qt::AlignRight);
    transPanelLayout2->addWidget(transYSpin, 1, 1, Qt::AlignRight);
    transPanelLayout2->addWidget(transZSpin, 2, 1, Qt::AlignRight);

    transPanelLayout->addLayout(transPanelLayout2);
    transPanel->setLayout(transPanelLayout);

    rotPanel = new QGroupBox(tr("Rotacja"), modelTab);

    angleL = new QLabel(tr("Kąt:"), rotPanel);
    angleSpin = new QDoubleSpinBox(rotPanel);
    rotButton = new QPushButton(tr("OK"), rotPanel);
    QHBoxLayout* rotTopLayout = new QHBoxLayout;
    rotTopLayout->addWidget(angleL);
    rotTopLayout->addWidget(angleSpin);
    rotTopLayout->addWidget(rotButton);

    rotXL = new QLabel(tr("x:"), rotPanel);
    rotYL = new QLabel(tr("y:"), rotPanel);
    rotZL = new QLabel(tr("z:"), rotPanel);
    rotXSpin = new QDoubleSpinBox(rotPanel);
    rotYSpin = new QDoubleSpinBox(rotPanel);
    rotZSpin = new QDoubleSpinBox(rotPanel);

    QVBoxLayout *rotPanelLayout = new QVBoxLayout;
    rotPanelLayout->addLayout(rotTopLayout);
    QGridLayout *rotPanelLayout2 = new QGridLayout;
    rotPanelLayout2->addWidget(rotXL, 0, 0);
    rotPanelLayout2->addWidget(rotYL, 1, 0);
    rotPanelLayout2->addWidget(rotZL, 2, 0);
    rotPanelLayout2->addWidget(rotXSpin, 0, 1, Qt::AlignRight);
    rotPanelLayout2->addWidget(rotYSpin, 1, 1, Qt::AlignRight);
    rotPanelLayout2->addWidget(rotZSpin, 2, 1, Qt::AlignRight);

    rotPanelLayout->addLayout(rotPanelLayout2);
    rotPanel->setLayout(rotPanelLayout);

    scalePanel = new QGroupBox(tr("Skalowanie"), modelTab);
    scaleAllCB = new QCheckBox(tr("Wszytkie jednakowe wartości"), scalePanel);
    scaleAllCB->setChecked(true);
    scaleXL = new QLabel(tr("x:"), scalePanel);
    scaleYL = new QLabel(tr("y:"), scalePanel);
    scaleZL = new QLabel(tr("z:"), scalePanel);
    scaleXSpin = new QDoubleSpinBox(scalePanel);
    scaleYSpin = new QDoubleSpinBox(scalePanel);
    scaleZSpin = new QDoubleSpinBox(scalePanel);

    QVBoxLayout *scalePanelLayout = new QVBoxLayout;
    scalePanelLayout->addWidget(scaleAllCB);
    QGridLayout *scalePanelLayout2 = new QGridLayout;
    scalePanelLayout2->addWidget(scaleXL, 0, 0);
    scalePanelLayout2->addWidget(scaleYL, 1, 0);
    scalePanelLayout2->addWidget(scaleZL, 2, 0);
    scalePanelLayout2->addWidget(scaleXSpin, 0, 1, Qt::AlignRight);
    scalePanelLayout2->addWidget(scaleYSpin, 1, 1, Qt::AlignRight);
    scalePanelLayout2->addWidget(scaleZSpin, 2, 1, Qt::AlignRight);

    scalePanelLayout->addLayout(scalePanelLayout2);
    scalePanel->setLayout(scalePanelLayout);



    QHBoxLayout* modelTabLayout = new QHBoxLayout;
    modelTabLayout->addWidget(transPanel);
    modelTabLayout->addWidget(scalePanel);
    modelTabLayout->addWidget(rotPanel);
    modelTabLayout->addStretch(3);



    modelTab->setLayout(modelTabLayout);

    //----------------------------------------------------------------------

    QColor color(255, 0, 0, 255);
    int minWidth = 50;

    QHBoxLayout* lightTabLayout = new QHBoxLayout;

    QVBoxLayout* lightOnAndSourceLayout = new QVBoxLayout;
    lightOnCB = new QCheckBox(tr("Włącz światło"), lightTab);
    drawLightSourceCB = new QCheckBox(tr("Rysuj źródło"), lightTab);
    lightOnAndSourceLayout->addWidget(lightOnCB);
    lightOnAndSourceLayout->addWidget(drawLightSourceCB);

    QGridLayout* lightOptionLayout = new QGridLayout;
    //lightOptionLayout->addWidget(lightOnCB, 0, 0);
    lightOptionLayout->addLayout(lightOnAndSourceLayout, 0, 0, Qt::AlignCenter);
    //lightOptionLayout->setMargin(0);

    ambientPanel = new QGroupBox(tr("Ambient"), lightTab);
    ambientColorL = new QLabel(tr("Kolor:"), ambientPanel);
    ambientColorB = new ColorButton(color, ambientPanel);
    ambientColorB->setFixedWidth(minWidth);

    QGridLayout* ambientLayout = new QGridLayout;
    ambientLayout->addWidget(ambientColorL, 0, 0);
    ambientLayout->addWidget(ambientColorB, 0, 1, Qt::AlignRight);
    ambientPanel->setLayout(ambientLayout);

    lightOptionLayout->addWidget(ambientPanel, 1, 0); //, Qt::AlignCenter);

    QHBoxLayout* diffAndSpecLayout = new QHBoxLayout;
    diffPanel = new QGroupBox(tr("Diffuse"), lightTab);
    diffColorL = new QLabel(tr("Kolor:"), diffPanel);
    diffColorB = new ColorButton(color, diffPanel);
    diffColorB->setFixedWidth(minWidth);

    QGridLayout* diffLayout = new QGridLayout;
    diffLayout->addWidget(diffColorL, 0, 0);
    diffLayout->addWidget(diffColorB, 0, 1, Qt::AlignRight);
    diffPanel->setLayout(diffLayout);

    diffAndSpecLayout->addWidget(diffPanel);

    specPanel = new QGroupBox(tr("Specular"), lightTab);
    specColorL = new QLabel(tr("Kolor:"), specPanel);
    specColorB = new ColorButton(color, specPanel);
    specColorB->setFixedWidth(minWidth);

    QGridLayout* specLayout = new QGridLayout;
    specLayout->addWidget(specColorL, 0, 0);
    specLayout->addWidget(specColorB, 0, 1, Qt::AlignRight);
    specPanel->setLayout(specLayout);

    diffAndSpecLayout->addWidget(specPanel);

    lightPosPanel = new QGroupBox(tr("Pozycja"), lightTab);
    lightPosXL = new QLabel(tr("x:"), lightPosPanel);
    lightPosYL = new QLabel(tr("y:"), lightPosPanel);
    lightPosZL = new QLabel(tr("z:"), lightPosPanel);
    lightPosXSpin = new QDoubleSpinBox(lightPosPanel);
    lightPosYSpin = new QDoubleSpinBox(lightPosPanel);
    lightPosZSpin = new QDoubleSpinBox(lightPosPanel);

    QHBoxLayout* lightPosLayout = new QHBoxLayout;

    //lightPosLayout->addWidget(lightPosL);
    lightPosLayout->addWidget(lightPosXL);
    lightPosLayout->addWidget(lightPosXSpin);
    lightPosLayout->addWidget(lightPosYL);
    lightPosLayout->addWidget(lightPosYSpin);
    lightPosLayout->addWidget(lightPosZL);
    lightPosLayout->addWidget(lightPosZSpin);

    lightPosPanel->setLayout(lightPosLayout);

    QVBoxLayout* diffSpecPosLayout = new QVBoxLayout;
    diffSpecPosLayout->addLayout(diffAndSpecLayout);
    diffSpecPosLayout->addWidget(lightPosPanel);


    lightTabLayout->addLayout(lightOptionLayout);
    lightTabLayout->addLayout(diffSpecPosLayout);

    lightTabLayout->addStretch(1);

    lightTab->setLayout(lightTabLayout);

    //-------------------------------------------------------
    Point4f& lightPos = glWidget->getLightPosition();
    double singleStep = 1.0;
    double min = -10000000.0;
    double max = 10000000.0;
    lightPosXSpin->setSingleStep(singleStep);
    lightPosXSpin->setMaximum(max);
    lightPosXSpin->setMinimum(min);
    lightPosXSpin->setValue(lightPos.x);

    lightPosYSpin->setSingleStep(singleStep);
    lightPosYSpin->setMaximum(max);
    lightPosYSpin->setMinimum(min);
    lightPosYSpin->setValue(lightPos.y);

    lightPosZSpin->setSingleStep(singleStep);
    lightPosZSpin->setMaximum(max);
    lightPosZSpin->setMinimum(min);
    lightPosZSpin->setValue(lightPos.z);

    Color4f& ambiCol = glWidget->getAmbientColor();
    ambientColorB->setColor(ambiCol.r, ambiCol.g, ambiCol.b);

    Color4f& diffCol = glWidget->getDiffuseColor();
    diffColorB->setColor(diffCol.r, diffCol.g, diffCol.b);

    Color4f& specCol = glWidget->getAmbientColor();
    specColorB->setColor(specCol.r, specCol.g, specCol.b);

    lightOnCB->setChecked(glWidget->isLightOn());
    drawLightSourceCB->setChecked(glWidget->isDrawLightSource());

    transXSpin->setSingleStep(singleStep);
    transXSpin->setMaximum(max);
    transXSpin->setMinimum(min);
    transYSpin->setSingleStep(singleStep);
    transYSpin->setMaximum(max);
    transYSpin->setMinimum(min);
    transZSpin->setSingleStep(singleStep);
    transZSpin->setMaximum(max);
    transZSpin->setMinimum(min);

    rotXSpin->setSingleStep(singleStep);
    rotXSpin->setMaximum(max);
    rotXSpin->setMinimum(min);
    rotYSpin->setSingleStep(singleStep);
    rotYSpin->setMaximum(max);
    rotYSpin->setMinimum(min);
    rotZSpin->setSingleStep(singleStep);
    rotZSpin->setMaximum(max);
    rotZSpin->setMinimum(min);

    scaleXSpin->setSingleStep(singleStep);
    scaleXSpin->setMaximum(max);
    scaleXSpin->setMinimum(min);
    scaleYSpin->setSingleStep(singleStep);
    scaleYSpin->setMaximum(max);
    scaleYSpin->setMinimum(min);
    scaleZSpin->setSingleStep(singleStep);
    scaleZSpin->setMaximum(max);
    scaleZSpin->setMinimum(min);

    scaleXSpin->setValue(1.0);
    scaleYSpin->setValue(1.0);
    scaleZSpin->setValue(1.0);

    angleSpin->setSingleStep(singleStep);
    angleSpin->setMaximum(max);
    angleSpin->setMinimum(min);

    rotXSpin->setValue(1.0);
    angleSpin->setValue(45.0);
}

void MainWindow::connectSlots() {
    QObject::connect(ambientColorB, SIGNAL(clicked()), this, SLOT(ambientColorBAction()));
    QObject::connect(diffColorB, SIGNAL(clicked()), this, SLOT(diffColorBAction()));
    QObject::connect(specColorB, SIGNAL(clicked()), this, SLOT(specColorBAction()));

    QObject::connect(lightPosXSpin, SIGNAL(valueChanged(double)), this,
            SLOT(lightPosChanged(double)));
    QObject::connect(lightPosYSpin, SIGNAL(valueChanged(double)), this,
            SLOT(lightPosChanged(double)));
    QObject::connect(lightPosZSpin, SIGNAL(valueChanged(double)), this,
            SLOT(lightPosChanged(double)));

    QObject::connect(lightOnCB, SIGNAL(stateChanged(int)), this,
            SLOT(lightOnStateChanged(int)));

    QObject::connect(drawLightSourceCB, SIGNAL(stateChanged(int)), this,
            SLOT(drawLightStateChanged(int)));

    QObject::connect(meshTree, SIGNAL(
            itemCheckedAction(const vector<TreeItem*>&, const vector<TreeItem*>&)),
            glWidget, SLOT(
            treeItemCheckedAction(const vector<TreeItem*>&, const vector<TreeItem*>&)));

    QObject::connect(meshTree, SIGNAL(
            itemCheckedAction(const vector<TreeItem*>&, const vector<TreeItem*>&)),
            this, SLOT(
            itemCheckedAction(const vector<TreeItem*>&, const vector<TreeItem*>&)));

    //    QObject::connect(meshTree, SIGNAL(itemCheckedAction(const vector<TreeItem*>&)),
    //            this, SLOT(treeItemCheckedAction(const vector<TreeItem*>&)));

    QObject::connect(transXSpin, SIGNAL(valueChanged(double)), this,
            SLOT(translateAction(double)));
    QObject::connect(transYSpin, SIGNAL(valueChanged(double)), this,
            SLOT(translateAction(double)));
    QObject::connect(transZSpin, SIGNAL(valueChanged(double)), this,
            SLOT(translateAction(double)));

    QObject::connect(scaleXSpin, SIGNAL(valueChanged(double)), this,
            SLOT(scaleAction(double)));
    QObject::connect(scaleYSpin, SIGNAL(valueChanged(double)), this,
            SLOT(scaleAction(double)));
    QObject::connect(scaleZSpin, SIGNAL(valueChanged(double)), this,
            SLOT(scaleAction(double)));


    QObject::connect(rotButton, SIGNAL(clicked()), this, SLOT(rotOkButtonAction()));
}

void MainWindow::openFileMenuAction() {
    QString filename = QFileDialog::getOpenFileName(
            this,
            tr("Otwórz"),
            QDir::currentPath(),
            tr("Modele 3DS (*.3ds)"));
    if (!filename.isEmpty()) {
        cout << "filename = " << filename.toStdString() << endl;

        loader = glWidget->loadModel(filename.toStdString().c_str());
        if (loader == NULL) {
            cout << "loader = " << loader << "; loader->getModel() == " << loader->getModel() << endl;
            QMessageBox::critical(
                    this,
                    tr("Błąd!"),
                    tr("Nie moge wczytać modelu! Błędny plik?"));
            return;
        }

        meshTree->setTree(loader->getModel()->getModelName(),
                loader->getMeshInfoGraph());

        meshTree->expandAll();

        canChangePosition = false;
        Point4f& lightPos = glWidget->getLightPosition();
        lightPosXSpin->setValue(lightPos.x);
        lightPosYSpin->setValue(lightPos.y);
        lightPosZSpin->setValue(lightPos.z);
        canChangePosition = true;

        clearSpinners();
    }
}

void MainWindow::aboutMenuAction() {
    QMessageBox::information(
            this,
            tr("My3dsViewer"),
            tr("My3dsViewer 0.1\nAutor: Kamil Kolaczynski"));
}

void MainWindow::drawLocalCoordsMenuAction() {
    glWidget->setDrawLocalCoordSystem(drawLocalCoordsAction->isChecked());
}

void MainWindow::drawObjectFrameMenuAction() {
    glWidget->setDrawObjectFrame(drawObjectFrameAction->isChecked());
}

void MainWindow::drawCoordSystemMenuAction() {
    glWidget->setDrawCoordSystem(drawCoordSystemAction->isChecked());
}

void MainWindow::showGridMenuAction() {
    glWidget->setWireMode(showGridAction->isChecked());
    // glWidget->repaint();
}

void MainWindow::changeBgColorMenuAction() {
    QColor color = QColorDialog::getColor(glWidget->getBgColor(), this);
    if (color.isValid()) {
        glWidget->setBgColor(color);
        //  glWidget->repaint();
        //qDebug("ok");
    }
}

void MainWindow::exitMenuAction() {
    exit(0);
}

void MainWindow::enlightObjectsMenuAction() {
    glWidget->setEnlightSelection(enlightObjectsAction->isChecked());
}

void MainWindow::drawOnlySelectedMenuAction() {
    glWidget->setDrawOnlySelected(drawOnlySelectedAction->isChecked());
}

void MainWindow::createMenu() {
    openFileAction = new QAction(tr("&Otwórz"), this);
    openFileAction->setShortcuts(QKeySequence::Open);
    openFileAction->setStatusTip(tr("Otwórz plik .3ds"));

    exitAction = new QAction(tr("&Wyjście"), this);
    exitAction->setShortcuts(QKeySequence::Quit);
    exitAction->setStatusTip(tr("Zamknij program"));

    aboutAction = new QAction(tr("O &Programie..."), this);
    //aboutAction->setShortcuts(QKeySequence::);
    aboutAction->setStatusTip(tr("Informacje na temat tego programu"));


    fileMenu = menuBar()->addMenu(tr("&Plik"));
    fileMenu->addAction(openFileAction);
    fileMenu->addSeparator();
    fileMenu->addAction(exitAction);

    viewMenu = menuBar()->addMenu(tr("&Widok"));
    showGridAction = new QAction(tr("&Pokaż siatkę"), this);
    showGridAction->setCheckable(true);
    changeBgColorAction = new QAction(tr("&Kolor tła..."), this);

    enlightObjectsAction = new QAction(tr("P&odświetl modyfikowane obiekty"), this);
    enlightObjectsAction->setCheckable(true);

    drawOnlySelectedAction = new QAction(tr("&Rysuj tylko zaznaczone"), this);
    drawOnlySelectedAction->setCheckable(true);

    drawCoordSystemAction = new QAction(tr("Ry&suj układ współrzędnych"), this);
    drawCoordSystemAction->setCheckable(true);

    drawLocalCoordsAction = new QAction(tr("Rys&uj lokalny układ współrzędnych"), this);
    drawLocalCoordsAction->setCheckable(true);

    drawObjectFrameAction = new QAction(tr("Rysu&j ramkę dla zaznaczonych"), this);
    drawObjectFrameAction->setCheckable(true);

    viewMenu->addAction(showGridAction);
    viewMenu->addSeparator();
    viewMenu->addAction(enlightObjectsAction);
    viewMenu->addAction(drawOnlySelectedAction);
    viewMenu->addSeparator();
    viewMenu->addAction(drawCoordSystemAction);
    viewMenu->addAction(drawLocalCoordsAction);
    viewMenu->addAction(drawObjectFrameAction);
    viewMenu->addSeparator();
    viewMenu->addAction(changeBgColorAction);


    helpMenu = menuBar()->addMenu(tr("P&omoc"));
    helpMenu->addAction(aboutAction);

    connect(openFileAction, SIGNAL(triggered()), this, SLOT(openFileMenuAction()));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(exitMenuAction()));
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(aboutMenuAction()));
    connect(changeBgColorAction, SIGNAL(triggered()),
            this, SLOT(changeBgColorMenuAction()));

    connect(showGridAction, SIGNAL(triggered()), this, SLOT(showGridMenuAction()));
    connect(enlightObjectsAction, SIGNAL(triggered()),
            this, SLOT(enlightObjectsMenuAction()));
    connect(drawOnlySelectedAction, SIGNAL(triggered()),
            this, SLOT(drawOnlySelectedMenuAction()));

    connect(drawCoordSystemAction, SIGNAL(triggered()),
            this, SLOT(drawCoordSystemMenuAction()));

    connect(drawLocalCoordsAction, SIGNAL(triggered()),
            this, SLOT(drawLocalCoordsMenuAction()));

    connect(drawObjectFrameAction, SIGNAL(triggered()),
            this, SLOT(drawObjectFrameMenuAction()));
}

void MainWindow::ambientColorBAction() {
    //cout << "Clicked" << endl;
    QColor color = QColorDialog::getColor(ambientColorB->getColor(), this);
    if (color.isValid()) {
        ambientColorB->setColor(color);
        glWidget->setAmbientColor(color.redF(), color.greenF(), color.blueF());
    }


}

void MainWindow::diffColorBAction() {
    QColor color = QColorDialog::getColor(diffColorB->getColor(), this);
    if (color.isValid()) {
        diffColorB->setColor(color);
        glWidget->setDiffuseColor(color.redF(), color.greenF(), color.blueF());
    }
}

void MainWindow::specColorBAction() {
    QColor color = QColorDialog::getColor(specColorB->getColor(), this);
    if (color.isValid()) {
        specColorB->setColor(color);
        glWidget->setSpecularColor(color.redF(), color.greenF(), color.blueF());
        //qDebug("ok");
    }
}

void MainWindow::lightPosChanged(double d) {
    if (canChangePosition) {
        glWidget->setLightPosition(lightPosXSpin->value(), lightPosYSpin->value(),
                lightPosZSpin->value());
    }
}

void MainWindow::lightOnStateChanged(int state) {
    if (state == Qt::Checked) {
        glWidget->setLightOn(true);
    } else {
        glWidget->setLightOn(false);
    }
}

void MainWindow::itemCheckedAction(const vector<TreeItem*>& selectedItems,
        const vector<TreeItem*>& unselectedItems) {
    clearSpinners();
}

void MainWindow::drawLightStateChanged(int state) {
    if (state == Qt::Checked) {
        glWidget->setDrawLightSource(true);
    } else {
        glWidget->setDrawLightSource(false);
    }
}

void MainWindow::translateAction(double d) {

    if (canChangeTrans == false)
        return;

    if (transAllCB->isChecked()) {
        canChangeTrans = false;
        transXSpin->setValue(d);
        transYSpin->setValue(d);
        transZSpin->setValue(d);
        canChangeTrans = true;
    }

    float x = transXSpin->value() - oldTransPoint.x;
    float y = transYSpin->value() - oldTransPoint.y;
    float z = transZSpin->value() - oldTransPoint.z;

    glWidget->translateSelected(x, y, z);
    oldTransPoint.set(transXSpin->value(), transYSpin->value(), transZSpin->value());
    glWidget->repaint();
}

void MainWindow::rotOkButtonAction() {

    if (canChangeRot == false)
        return;

    float x = rotXSpin->value();
    float y = rotYSpin->value();
    float z = rotZSpin->value();
    float angle = angleSpin->value();

    glWidget->rotateSelected(angle, x, y, z);
    glWidget->repaint();
}

void MainWindow::scaleAction(double d) {
    if (d == 0.0) {
        QMessageBox::warning(
                this,
                tr("Pomijam zero!"),
                tr("Pomijam przypadek skalowania przez zero!"));
        return;
    }

    if (canChangeScale == false)
        return;

    if (scaleAllCB->isChecked()) {
        canChangeScale = false;
        scaleXSpin->setValue(d);
        scaleYSpin->setValue(d);
        scaleZSpin->setValue(d);
        canChangeScale = true;
    }

    glWidget->scaleSelected(1.0f / oldScalePoint.x, 1.0f / oldScalePoint.y,
            1.0f / oldScalePoint.z);

    //    float x = scaleXSpin->value() - oldScalePoint.x;
    //    float y = scaleYSpin->value() - oldScalePoint.y;
    //    float z = scaleZSpin->value() - oldScalePoint.z;

    glWidget->scaleSelected(scaleXSpin->value(), scaleYSpin->value(),
            scaleZSpin->value());
    oldScalePoint.set(scaleXSpin->value(), scaleYSpin->value(), scaleZSpin->value());
    glWidget->repaint();
}

void MainWindow::clearSpinners() {

    canChangeRot = false;
    canChangeScale = false;
    canChangeTrans = false;

    transXSpin->setValue(0.0);
    transYSpin->setValue(0.0);
    transZSpin->setValue(0.0);

    angleSpin->setValue(45.0);
    rotXSpin->setValue(1.0);
    rotYSpin->setValue(0.0);
    rotZSpin->setValue(0.0);

    scaleXSpin->setValue(1.0);
    scaleYSpin->setValue(1.0);
    scaleZSpin->setValue(1.0);

    canChangeRot = true;
    canChangeScale = true;
    canChangeTrans = true;

    oldScalePoint.set(1, 1, 1);
    oldTransPoint.set(0, 0, 0);
}

