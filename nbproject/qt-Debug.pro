# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Debug/MinGW-Windows
TARGET = My3dsViewerQT
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += debug 
PKGCONFIG +=
QT = core gui opengl
SOURCES += MainWindow.cpp stb_image_aug.c QMeshInfoTree.cpp BorderLayout.cpp main.cpp SOIL.c GLWidget.cpp ColorButton.cpp image_helper.c ArcBall.cpp treeitem.cpp image_DXT.c treemodel.cpp
HEADERS += image_DXT.h QMeshInfoTree.h stbi_DDS_aug_c.h GLWidget.h image_helper.h treeitem.h stb_image_aug.h stbi_DDS_aug.h MainWindow.h ColorButton.h ArcBall.h BorderLayout.h SOIL.h treemodel.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Debug/MinGW-Windows
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc.exe
QMAKE_CXX = g++.exe
DEFINES += 
INCLUDEPATH += include 
LIBS += D:/Revers/NetBeansProjects/My3dsViewerQT/libs/libmy3dsloaderlib.a D:/Revers/NetBeansProjects/My3dsViewerQT/libs/libQtOpenGLd4.a -lopengl32 -lfreeglut  
