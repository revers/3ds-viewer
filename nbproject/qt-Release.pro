# This file is generated automatically. Do not edit.
# Use project properties -> Build -> Qt -> Expert -> Custom Definitions.
TEMPLATE = app
DESTDIR = dist/Release/MinGW-Windows
TARGET = My3dsViewerQT
VERSION = 1.0.0
CONFIG -= debug_and_release app_bundle lib_bundle
CONFIG += release 
PKGCONFIG +=
QT = core gui
SOURCES += MainWindow.cpp stb_image_aug.c QMeshInfoTree.cpp BorderLayout.cpp main.cpp SOIL.c GLWidget.cpp ColorButton.cpp image_helper.c ArcBall.cpp treeitem.cpp image_DXT.c treemodel.cpp
HEADERS += image_DXT.h QMeshInfoTree.h stbi_DDS_aug_c.h GLWidget.h image_helper.h treeitem.h stb_image_aug.h stbi_DDS_aug.h MainWindow.h ColorButton.h ArcBall.h BorderLayout.h SOIL.h treemodel.h
FORMS +=
RESOURCES +=
TRANSLATIONS +=
OBJECTS_DIR = build/Release/MinGW-Windows
MOC_DIR = 
RCC_DIR = 
UI_DIR = 
QMAKE_CC = gcc.exe
QMAKE_CXX = g++.exe
DEFINES += NDEBUG 
INCLUDEPATH += ../RevLib ../My3dsLoaderLib ../../../Qt2/2010.05/qt/include/QtOpenGL 
LIBS += ../My3dsLoaderLib/dist/Release/MinGW-Windows/libmy3dsloaderlib.a C:/MinGW/lib/opengl/libopengl32.a C:/MinGW/lib/opengl/libglu32.a ../../../Qt2/2010.05/qt/lib/libQtOpenGL4.a  
