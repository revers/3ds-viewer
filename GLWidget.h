#ifndef GLWIDGET_H
#define GLWIDGET_H

# include <GL/glu.h>
#include <QGLWidget>
#include <iostream>
//#include <vector>
#include <map>

#include "Object3D.h"
#include "My3dsLoader.h"
#include "ArcBall.h"

#include "RevMath.h"
#include "RevPoint.h"
#include "RevColor.h"
//#include "treeitem.h"


using rev::graph::Point3f;
using rev::graph::Point4f;
using rev::graph::Color4f;
using std::vector;

typedef std::map<const char*, unsigned int> TextureMap;
typedef TextureMap::iterator TextureMapIterator;

class TreeItem;

class GLWidget : public QGLWidget {

    Q_OBJECT
private:
    struct ExtremePoints {
        // gorna podstawa
        Point3f a, b, c, d;
        // dolna podstawa;
        Point3f e, f, g, h;
        // punkt centralny;
        Point3f central;
    };
public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
public:
    void translateSelected(float x, float y, float z);
    void rotateSelected(float angle, float x, float y, float z);
    void scaleSelected(float x, float y, float z);

public:
    My3dsLoader* loadModel(const char* filename);

public slots:
    //void treeItemCheckedAction(const std::vector<TreeItem*>& checkedItems);
    void treeItemCheckedAction(const vector<TreeItem*>& checkedItems,
            const vector<TreeItem*>& uncheckedItems);

signals:
    void refreshHierarchy();
public:

    QColor& getBgColor() {
        return bgColor;
    }

    void setBgColor(QColor bgColor) {
        this->bgColor = bgColor;
        repaint();
    }

    void setWireMode(bool activated) {
        this->wireMode = activated;
        if (wireMode)
            glPolygonMode(GL_FRONT, GL_LINE);
        else
            glPolygonMode(GL_FRONT, GL_FILL);
        repaint();
    }

    bool isWireMode() const {
        return wireMode;
    }

    Color4f& getAmbientColor() {
        return ambientColor;
    }

    void setAmbientColor(float r, float g, float b) {
        ambientColor.set(r, g, b, 1.0f);
        processLight();
        // repaint();
    }

    Color4f& getDiffuseColor() {
        return diffuseColor;
    }

    void setDiffuseColor(float r, float g, float b) {
        diffuseColor.set(r, g, b, 1.0f);
        processLight();
        // repaint();
    }

    Color4f& getSpecularColor() {
        return specularColor;
    }

    void setSpecularColor(float r, float g, float b) {
        specularColor.set(r, g, b, 1.0f);
        processLight();
        //   repaint();
    }

    Point4f& getLightPosition() {
        return lightPosition;
    }

    void setLightPosition(float x, float y, float z) {
        lightPosition.x = x;
        lightPosition.y = y;
        lightPosition.z = z;
        repaint();
    }

    bool isDrawLightSource() const {
        return drawLightSource;
    }

    void setDrawLightSource(bool drawLight) {
        this->drawLightSource = drawLight;
        repaint();
    }

    bool isLightOn() const {
        return lightOn;
    }

    void setLightOn(bool lightOn) {
        this->lightOn = lightOn;
        repaint();
    }

    bool isEnlightSelection() const {
        return enlightSelection;
    }

    void setEnlightSelection(bool enlightSelection) {
        this->enlightSelection = enlightSelection;
        repaint();
    }

    bool isDrawOnlySelected() const {
        return drawOnlySelected;
    }

    void setDrawOnlySelected(bool drawOnlySelected) {
        this->drawOnlySelected = drawOnlySelected;
        repaint();
    }

    bool isDrawCoordSystem() const {
        return drawCoordSystem;
    }

    void setDrawCoordSystem(bool drawCoordSystem) {
        this->drawCoordSystem = drawCoordSystem;
        repaint();
    }

    bool isDrawLocalCoordSystem() const {
        return drawLocalCoordSystem;
    }

    void setDrawLocalCoordSystem(bool drawLocalCoordSystem) {
        this->drawLocalCoordSystem = drawLocalCoordSystem;
        repaint();
    }

    bool isDrawObjectFrame() const {
        return drawObjectFrame;
    }

    void setDrawObjectFrame(bool drawObjectFrame) {
        this->drawObjectFrame = drawObjectFrame;
        repaint();
    }

private:

    void initArcBall();

    void drawLocalObjectsFrame();

    void drawLocalCoordAxes();

    void drawLight();

    void setExtremePoints(Point3f& minPoint, Point3f& maxPoint);

    void handleHits(GLint hits, GLuint *names);

    void glSelection(int x, int y);

    void drawEveryObjectFrame(Object3D* o3d);

    void selectionChangedAction();

    //void calcLocalAxes();

    void drawCoordAxes();

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent * event);
    void mouseMoveEvent(QMouseEvent *event);
    void wheelEvent(QWheelEvent * event);

private:
    void processCamera();
    void processLight();
    void resetAll();
private:
    bool drawObjectFrame;
    bool drawLocalCoordSystem;

    Point3f translation;
    Point3f scale;
    Quaternionf rotation;

    //Point3f origCentral;

    //Matrix44f transformMatrix;
    //    Vector3f localXAxis;
    //    Vector3f localYAxis;
    //    Vector3f localZAxis;
    std::vector<Object3D*> selectedObjects;
    bool drawOnlySelected;

    Color4f selectionColor;
    bool enlightSelection;
    float oldAmbiColor[4];
    float oldSpecColor[4];
    float oldDiffColor[4];

    GLUquadricObj *lightSphere;
    bool wireMode;
    bool firstShow;
    QColor bgColor;

    Color4f ambientColor;
    Color4f diffuseColor;
    Color4f specularColor;

    Point4f lightPosition;

    bool lightOn;
    bool drawLightSource;
    bool drawCoordSystem;

    ExtremePoints extremePoints;
private:

    bool leftMBPressed;
    bool middleMBPressed;
    bool rightMBPressed;

    QPoint mousePoint;

    float cameraDistance;

    //float cameraPhiAngle, cameraThetaAngle;
    float cameraX, cameraY, cameraZ;
    float cameraLocation[3];

    Matrix4fT Transform;
    Matrix3fT LastRot;
    Matrix3fT ThisRot;
    ArcBallT* ArcBall;
    Point2fT MousePt;


private:
    TextureMap* textures;
    My3dsLoader *loader;
    //  void initLoader();
    //void destroyLoader();
    void initTextures();
    GLuint getTexture(Material* mat, const char* texturePath);
};


#endif
