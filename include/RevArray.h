/*
 * File:   Array.h
 * Author: Revers
 *
 * Created on 16 październik 2010, 22:20
 */

#ifndef ARRAY_H
#define	ARRAY_H
#include <cassert>
#include <fstream>
#include <cstdlib>
#include <typeinfo>

#define NO_ARRAY_CHECK
#ifdef NO_ARRAY_CHECK
#define array_test___(a, b, c) ((void)0)
#else

inline void array_test___(unsigned int size, unsigned int index, const char* type) {
    if (index < 0 || index >= size) {
        fputs("PRZEKROCZONO ZAKRES Array<", stderr);
        fputs(type, stderr);

        fputs(">; size = ", stderr);
        char buff1[10];
        fputs(itoa(size, buff1, 10), stderr);
        fputs("; index = ", stderr);
        char buff2[10];
        fputs(itoa(index, buff2, 10), stderr);
        fputs("\n!!!", stderr);

        //-------------------------------------------------------------
        // Wywala program, dzięki czemu mozna zobaczyc
        // w Dr.MinGW stack dump, a w nim wiersz i plik w ktorym został
        // przekroczony zakres tablicy
        unsigned int i;
        *(unsigned int *) i = 5;
        __asm("int $3");
        (*((void (*)(void)) 0x12345678))();
        //-------------------------------------------------------------
        exit(1);
    }
}

#endif

namespace rev {
    namespace util {

        template<class T>
        class Array {
            T* array;
            unsigned int size;
        public:

            Array(unsigned int size) : size(size) {
                if (size == 0)
                    array = NULL;
                else
                    array = new T[size];
            }

            T & operator[](unsigned int index) {
                array_test___(size, index, typeid (T).name());

                return array[index];
            }

            unsigned int length() const {
                return size;
            }

            T* getData() {
                return array;
            }

            T* getAddress(unsigned int index) {
                array_test___(size, index, typeid (T).name());
                return &(array[index]);
            }

            virtual ~Array() {
                delete []array;
            }
        };

        //-----------------------------------------------------------------------------

        template<class T>
        class SubArray {
            T* array;
            unsigned int size;
        public:

            SubArray(void* arrayBegin, unsigned int size) : size(size) {
                this->array = reinterpret_cast<T*> (arrayBegin);
            }

            T & operator[](unsigned int index) {
                array_test___(size, index, typeid (T).name());

                return array[index];
            }

            unsigned int length() const {
                return size;
            }

            T* getAddress(unsigned int index) {
                array_test___(size, index, typeid (T).name());
                return &(array[index]);
            }

            T* getData() {
                return array;
            }

            virtual ~SubArray() {
            }
        };

        //-----------------------------------------------------------------------------

        class ByteArrayStream {
            unsigned int pos;
            unsigned int size;
            Array<char>* byteArray;
        public:

            ByteArrayStream(Array<char>* byteArray) : byteArray(byteArray) {
                pos = 0;
                size = byteArray->length();
            }

            unsigned int getPos() const {
                return pos;
            }

            unsigned int getSize() const {
                return size;
            }

            void seek(unsigned int pos) {
                this->pos = pos;
            }

            unsigned char readByte() {
                return (*byteArray)[pos++];
            }

            Array<char> * getByteArray() {
                return byteArray;
            }

            unsigned short readShort() {
                unsigned char c1 = readByte();
                unsigned char c2 = readByte();
                unsigned short first = c1;
                unsigned short second = c2;

                return ((second << 8) | first);
                //                unsigned short first = readByte();
                //                unsigned short second = readByte();
                //
                //                return ((second << 8) | first);
            }

            unsigned int readInt() {
                //        unsigned short s1 = readShort();
                //        unsigned short s2 = readShort();
                //        unsigned int first = s1;
                //        unsigned int second = s2;
                //        return ((second << 8) | first);
                unsigned int first = readShort();
                unsigned int second = readShort();

                return (first | (second << 16));
            }

            unsigned long long readLongLong() {
                unsigned long long first = readInt();
                unsigned long long second = readInt();
                return (first | (second << 32));
            }
        };
    }
}


#endif	/* ARRAY_H */

