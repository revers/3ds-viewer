/* 
 * File:   My3dsLoader.h
 * Author: Revers
 *
 * Created on 19 październik 2010, 20:55
 */

#ifndef MY3DSLOADER_H
#define	MY3DSLOADER_H
//#include <stdexcept>
#include <vector>
#include <string>
#include "RevArray.h"
#include "Chunk.h"
#include "Object3D.h"
#include "Model3D.h"
#include "MeshInfoBlock.h"

using rev::util::Array;

const bool PRINT_DEBUG_INFO = false;

//class ChunkException : public std::runtime_error {
//public:
//
//    ChunkException(const char* msg) : std::runtime_error(msg) {
//    }
//};

typedef std::vector<Object3D*> Object3DVector;
typedef Array<Object3D*> Object3DArray;

typedef std::vector<Material*> MaterialVector;
typedef Array<Material*> MaterialArray;

#define DS_FILE_SEPARATOR '\\'

class My3dsLoader {
private:
    std::vector<MeshInfoBlock*> meshInfoVect;
    Array<MeshInfoBlock*>* meshInfoList;
    Array<MeshInfoBlock*>* meshInfoGraph;

    std::vector<DsNormal*>* normalVectorBuffer;
    bool* checkedVertexBuffer;
    int checkedVertexBufferSize;

private:
    Object3DVector* objVector;
    Object3DArray* objArray;
    Model3D* model;

    MaterialVector* materialVector;
    MaterialArray* materialArray;

    ChunkDescArray* chunkDescTree;
    ChunkArray* chunkTree;

    Array<char>* rawFileData;

    const char* filename;
    char* directory;
    char* modelName;

private:
    My3dsLoader(const char* filename, Array<char>* rawFileData,
            ChunkDescArray* chunkDescTree, ChunkArray* chunkTree);

    My3dsLoader(const My3dsLoader&) {
    }

private:
    void findExtremePoints(Object3D * o3d);

    void addChilds(MeshInfoBlock* parent,
            std::vector<MeshInfoBlock*>* remainderObjects);

    void printMeshGraph(Array<MeshInfoBlock*>* meshInfoGraph, int level);

    void generateMeshGraph(std::vector<MeshInfoBlock*>* meshVect);

    void addChilds(Object3D* parent,
            Object3DArray* remainderObjects);
    void printObjectGraph(Object3DArray* obj3DArray, int level = 0);
    Model3D* createModel3D(Object3DArray* o3dArr);

    void assignMaterialsToObjects();

    void assignObjectsToMeshInfos(ObjectList* list,
            Array<MeshInfoBlock*>* meshInfos);

    char* extractDirectory(const char* filename);

    inline char* createCString(std::string s);

    inline unsigned short readShort(ChunkData * data, unsigned int offset);

    inline unsigned int readInt(ChunkData* data, unsigned int offset);

    void getColor(Chunk* colorChunk, DsColor* result, unsigned int level = 0);

    int getPercent(Chunk* colorChunk, unsigned int level = 0);

    void printHex(ChunkData* data, unsigned int offset, unsigned int byteCount);

    void printMargin(int level);

    std::string& toBinaryString(unsigned int val, std::string& result);

    std::string& readString(ChunkData* data, unsigned int offset,
            std::string& result);

    bool parseMaterialChunk(Chunk* chunk); // throw (ChunkException);

    SubObjectTriangles* parseFacesDescription(Chunk* chunk);

    SubObjectTriangles* parseFacesMaterialList(Chunk* chunk,
            char* triangleDataBegin);

    void parseKeyframeChunk(Chunk *chunk);

    void parseMeshInformationChunk(Chunk* chunk);

    void parseSmoothingGroup(Chunk* chunk, unsigned int length);

    void parseLocalCoordSys(Chunk* chunk);

    Array<TexCoord>* parseTexMaping(Chunk* chunk);

    Array<Point3f>* parseVertices(Chunk* chunk);

    inline void normalizeVector(Vector3f& vect);

    inline void calcNormal(Point3f& aVert, Point3f& bVert,
            Point3f& cVert, Vector3f& result);

    Array<DsNormal>* createNormalArray(SubObjectTriangles* subObjTriangles,
            Array<Point3f>* vertices);

    bool parseTriangularMeshChunk(Chunk* triChunk, Object3D* o3d);
    //    throw (ChunkException);

    void parseObjectBlockChunk(Chunk* chunk);

    void parseEditorChunk(Chunk* chunk);

    void parseMainChunk(Chunk* chunk);

    void parseTextureMap(Chunk* chunk, Material* material);


    void interpolateNormals(SubObjectTriangles* subs, Array<Point3f>* vertices,
            Array<DsNormal>* normals, std::vector<DsNormal*>* normalVectorBuffer,
            bool* checkedVertexBuffer);

    void interpolateNormals(std::vector<DsNormal*> *normals);
    //void loadTextures();
public:

    Array<MeshInfoBlock*>* getMeshInfoGraph() const {
        return meshInfoGraph;
    }

    Array<MeshInfoBlock*>* getMeshInfoList() const {
        return meshInfoList;
    }

    virtual ~My3dsLoader();

    static Array<char>*
    getModelData(const char* filename); // throw (std::runtime_error);

    static My3dsLoader* loadModel(const char* filename);
    //throw (std::runtime_error);

    static void deleteObjects(Object3DArray* objects) {
        if (objects == NULL)
            return;
        for (unsigned int i = 0; i < objects->length(); i++) {
            delete (*objects)[i];
        }

        delete objects;
    }

    static void deleteMaterials(MaterialArray* materials) {
        if (materials == NULL)
            return;
        for (unsigned int i = 0; i < materials->length(); i++) {
            delete (*materials)[i];
        }
        delete materials;
    }

    void deleteAllCreatedData();

    Model3D* getModel() const {
        return model;
    }

    MaterialArray* getMaterials() const {
        return materialArray;
    }

    ChunkDescArray* getChunkDescTree() const {
        return chunkDescTree;
    }

    ChunkArray* getChunkTree() const {
        return chunkTree;
    }

    const char* getDirectory() const {
        return directory;
    }

    const char* getFilename() const {
        return filename;
    }
};

#endif	/* MY3DSLOADER_H */

