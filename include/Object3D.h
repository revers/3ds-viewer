/* 
 * File:   Object3D.h
 * Author: Revers
 *
 * Created on 19 październik 2010, 21:04
 */

#ifndef OBJECT3D_H
#define	OBJECT3D_H
#include <string>
#include "RevArray.h"
#include "RevGraphics.h"

#include "Material.h"


using namespace rev::graph;
using rev::util::Array;

#define HIERARCHY_NO_FATHER 65535

typedef Vector3f DsNormal;

struct DsLocalCoordSys {
    Vector3f xVect;
    Vector3f yVect;
    Vector3f zVect;
    Vector3f oVect;
};
////----------------------------------------------------------------------------

class SubObjectTriangles {
private:
    Array<ShortTriangleRef>* triangleArray;
    char* materialName;
    Material* material;
    SubObjectTriangles* nextSubObject;

private:

    void deleteAllList(SubObjectTriangles* subs) {
        if (subs->getNext() != NULL) {
            deleteAllList(subs->getNext());
        }

        delete subs;
    }

public:

    SubObjectTriangles(char* materialName, Array<ShortTriangleRef>* triangleArray) {
        this->triangleArray = triangleArray;
        this->materialName = materialName;
        this->material = NULL;
        nextSubObject = NULL;
    }

    Material* getMaterial() const {
        return material;
    }

    void setMaterial(Material* material) {
        this->material = material;
    }

    char* getMaterialName() const {
        return materialName;
    }

    Array<ShortTriangleRef>* getTriangleArray() const {
        return triangleArray;
    }

    ~SubObjectTriangles() {
        delete triangleArray;
        if (materialName != NULL) {
            delete materialName;
        }
    }

    void setNext(SubObjectTriangles* next) {
        this->nextSubObject = next;
    }

    SubObjectTriangles* getNext() const {
        return this->nextSubObject;
    }

    void deleteWholeList() {
        deleteAllList(this);
    }

    int getTrianglesCount() {
        return triangleArray->length();
    }

    int getWholeListTrianglesCount() {
        int count = triangleArray->length();
        SubObjectTriangles* subs = this;

        while ((subs = subs->getNext()) != NULL) {
            count += subs->getTrianglesCount();
        }

        return count;
    }
};

//-----------------------------------------------------------------------------

struct Bounds {
    // gorna podstawa
    Point3f a, b, c, d;
    // dolna podstawa;
    Point3f e, f, g, h;
};
//-----------------------------------------------------------------------------

class Object3D {
    int pickingId;
    const char *name;
    Array<Point3f>* vertexArray;
    Array<DsNormal>* normalArray;
    Array<TexCoord>* texCoordArray;
    SubObjectTriangles* subObjTriangles;
    Array<Object3D*>* subObjects;

    int hierarchyPosition;
    int hierarchyFather;
    //Matrix44f transformMatrix;
    Quaternionf rotation;
    Point3f translation;
    Point3f scale;

    // Point3f smallestPoint;
    //   Point3f biggestPoint;

    bool visible;
    bool selected;

    Bounds bounds;

public:

    Object3D(const char* name) :
    pickingId(0),
    name(name),
    vertexArray(NULL),
    normalArray(NULL),
    texCoordArray(NULL),
    subObjTriangles(NULL),
    subObjects(NULL),
    hierarchyPosition(-1),
    hierarchyFather(-1),
    //  transformMatrix(Matrix44f::createIdentityMatrix()),
    scale(1, 1, 1),
    visible(true),
    selected(true) {
    }

    Array<DsNormal>* getNormalArray() const {
        return normalArray;
    }

    Array<TexCoord>* getTexCoordArray() const {
        return texCoordArray;
    }

    SubObjectTriangles* getSubObjectTriangles() const {
        return subObjTriangles;
    }

    Array<Point3f>* getVertexArray() const {
        return vertexArray;
    }

    int getVertexCount() const {
        return vertexArray->length();
    }

    int getTriangleCount() const {
        return subObjTriangles->getWholeListTrianglesCount();
    }

    int getNormalCount() const {
        return normalArray->length();
    }

    int getTexCoordCount() const {
        return texCoordArray->length();
    }

    void setNormalArray(Array<DsNormal>* normalArray) {
        this->normalArray = normalArray;
    }

    void setTexCoordArray(Array<TexCoord>* texCoordArray) {
        this->texCoordArray = texCoordArray;
    }

    void setSubObjectTriangles(SubObjectTriangles* subObjTriangles) {
        this->subObjTriangles = subObjTriangles;
    }

    void setVertexArray(Array<Point3f>* vertexArray) {
        this->vertexArray = vertexArray;
    }

    const char* getName() const {
        return name;
    }

    virtual ~Object3D() {
        delete name;
        if (vertexArray != NULL) {
            delete vertexArray;
        }

        if (normalArray != NULL) {
            delete normalArray;
        }

        if (texCoordArray != NULL) {
            delete texCoordArray;
        }

        if (subObjTriangles != NULL) {
            subObjTriangles->deleteWholeList();
        }
    }

    void draw(bool ignoreMaterial = false);

    //    Point3f getBiggestPoint() {
    //        //  return biggestPoint;
    //
    //        Point3f result(biggestPoint);
    //
    //        Matrix44f transMat = Matrix44f::createTranslationMatrix(translationPoint);
    //        Matrix44f scaleMat = Matrix44f::createScaleMatrix(scalePoint);
    //        Matrix44f rotMat = rotationQuaternion.toMatrix44();
    //
    //        rotMat *= transMat;
    //        rotMat *= scaleMat;
    //
    //        result = rotMat * result;
    //
    //        //        float angle = rotationQuaternion.getRotationAngle();
    //        //        Vector3f axis = rotationQuaternion.getRotationVector();
    //        //        //  axis.normalize();
    //        //
    //        //        Matrix44f rotMat = Matrix44f::createRotationMatrix(axis, angle);
    //        //        result = rotMat*result;
    //        //
    //        //        result.x *= scalePoint.x;
    //        //        result.y *= scalePoint.y;
    //        //        result.z *= scalePoint.z;
    //        //
    //        //        result += translationPoint;
    //
    //        return result;
    //    }
    //
    //    void setBiggestPoint(Point3f biggestPoint) {
    //        this->biggestPoint = biggestPoint;
    //    }

    int getHierarchyFather() const {
        return hierarchyFather;
    }

    void setHierarchyFather(int hierarchyFather) {
        this->hierarchyFather = hierarchyFather;
    }

    //    Matrix44f& getTransformMatrix() {
    //        return transformMatrix;
    //    }
    //
    //    void setTransformMatrix(Matrix44f matrix) {
    //        this->transformMatrix = matrix;
    //    }
    //
    //    Point3f getSmallestPoint() {
    //        //return smallestPoint;
    //        Point3f result(smallestPoint);
    //
    //        Matrix44f transMat = Matrix44f::createTranslationMatrix(translationPoint);
    //        Matrix44f scaleMat = Matrix44f::createScaleMatrix(scalePoint);
    //        Matrix44f rotMat = rotationQuaternion.toMatrix44();
    //
    //        rotMat *= transMat;
    //        rotMat *= scaleMat;
    //
    //        result = rotMat * result;
    //
    //        //        float angle = rotationQuaternion.getRotationAngle();
    //        //        Vector3f axis = rotationQuaternion.getRotationVector();
    //        //        //axis.normalize();
    //        //
    //        //        Matrix44f rotMat = Matrix44f::createRotationMatrix(axis, angle);
    //        //        result = rotMat*result;
    //        //
    //        //        result.x *= scalePoint.x;
    //        //        result.y *= scalePoint.y;
    //        //        result.z *= scalePoint.z;
    //        //
    //        //        result += translationPoint;
    //
    //        return result;
    //    }

    //    void setSmallestPoint(Point3f smallestPoint) {
    //        this->smallestPoint = smallestPoint;
    //    }

    int getHierarchyPosition() const {
        return hierarchyPosition;
    }

    void setHierarchyPosition(int hierarchyPosition) {
        this->hierarchyPosition = hierarchyPosition;
    }

    bool isVisible() const {
        return visible;
    }

    void setVisible(bool visible) {
        this->visible = visible;
    }

    void deleteAllSubObjects() {
        if (subObjects == NULL)
            return;
        for (unsigned int i = 0; i < subObjects->length(); i++) {
            if ((*subObjects)[i] != NULL) {
                ((*subObjects)[i])->deleteAllSubObjects();
                delete ((*subObjects)[i]);
            }
        }
        delete subObjects;
    }

    void deleteAll() {
        deleteAllSubObjects();
        delete this;
    }

    Array<Object3D*>* getSubObjects() const {
        return subObjects;
    }

    void setSubObjects(Array<Object3D*>* subObjects) {
        this->subObjects = subObjects;
    }

    bool isSelected() const {
        return selected;
    }

    void setSelected(bool selected) {
        this->selected = selected;
    }

    Point3f& getScale() {
        return scale;
    }

    void setScale(Point3f scale) {
        this->scale = scale;
    }

    void setScale(float x, float y, float z) {
        scale.x = x;
        scale.y = y;
        scale.z = z;
    }

    Point3f& getTranslation() {
        return translation;
    }

    void setTranslation(Point3f translation) {
        this->translation = translation;
    }

    void setTranslation(float x, float y, float z) {
        translation.x = x;
        translation.y = y;
        translation.z = z;
    }

    void addScale(float x, float y, float z) {
        scale.x += x;
        scale.y += y;
        scale.z += z;
    }

    Quaternionf& getRotation() {
        return rotation;
    }

    void setRotation(Quaternionf rotationQuaternion) {
        this->rotation = rotationQuaternion;
    }

    int getPickingId() const {
        return pickingId;
    }

    void setPickingId(int pickingId) {
        this->pickingId = pickingId;
    }

    Bounds& getBounds() {
        return bounds;
    }

    void setBounds(Bounds bounds) {
        this->bounds = bounds;
    }

};

#endif	/* OBJECT3D_H */

