/* 
 * File:   Model3D.h
 * Author: Revers
 *
 * Created on 1 listopad 2010, 22:06
 */

#ifndef MODEL3D_H
#define	MODEL3D_H

#include "Object3D.h"

typedef Array<Object3D*> ObjectGraph;
typedef Array<Object3D*> ObjectList;

class Model3D {
private:
    char* modelName;
    ObjectGraph* objectGraph;
    ObjectList* objectList;
public:

    Model3D(char* modelName, ObjectGraph* objectGraph, ObjectList* objList) :
    modelName(modelName), objectGraph(objectGraph), objectList(objList) {
    }

    
    char* getModelName() const {
        return modelName;
    }

    void setModelName(char* modelName) {
        this->modelName = modelName;
    }

    ObjectGraph* getObjectGraph() const {
        return objectGraph;
    }
    
    ObjectList* getObjectList() const {
        return objectList;
    }

    ~Model3D() {
        if (objectList != NULL) {
            for (unsigned int i = 0; i < objectList->length(); i++) {
                delete (*objectList)[i];
            }
            delete objectList;
        }

        if (objectGraph != NULL) {
            delete objectGraph;
        }

        if (modelName != NULL) {
            delete modelName;
        }
    }
};


#endif	/* MODEL3D_H */

