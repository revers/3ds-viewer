/* 
 * File:   Material.h
 * Author: Revers
 *
 * Created on 24 październik 2010, 13:00
 */

#ifndef MATERIAL_H
#define	MATERIAL_H

#define COLOR_GAMMA_NOT_TRASPARENT 1.0f

struct DsColor {

    DsColor() {
        r = 0.0f;
        g = 0.0f;
        b = 0.0f;
        a = COLOR_GAMMA_NOT_TRASPARENT;
    }

    float r;
    float g;
    float b;
    float a;

//    unsigned char r;
//    unsigned char g;
//    unsigned char b;
//    unsigned char a;
};

class Material {
    const char* name;

    char* texturePath;
    //  Image* texture;
    bool textureLoaded;
    bool texture2Loaded;
    unsigned int textureGLid;

    char* texturePath2;
    //   Image* texture2;

    DsColor* ambientColor;
    DsColor* diffuseColor;
    DsColor* specularColor;

    int shininessPercent;
    int shininessStrengthPercent;
    int transparencyPercent;
    int transparencyFalloffPercent;
public:

    Material(const char* name) : name(name) {
        //  texture = NULL;
        //  texture2 = NULL;
        textureLoaded = false;
        texture2Loaded = false;

        ambientColor = NULL;
        diffuseColor = NULL;
        specularColor = NULL;

        texturePath = NULL;
        texturePath2 = NULL;

        shininessPercent = -1;
        shininessStrengthPercent = -1;
        transparencyPercent = -1;
        transparencyFalloffPercent = -1;

        textureGLid = 0;
    }

    bool hasTexture() {
        return texturePath != NULL;
    }

    //    bool isTextureLoaded() {
    //        return texture != NULL;
    //    }

    bool hasTexture2() {
        return texturePath2 != NULL;
    }

    //    bool isTexture2Loaded() {
    //        return texture2 != NULL;
    //    }

    bool hasAmbientColor() {
        return ambientColor != NULL;
    }

    bool hasDiffuseColor() {
        return diffuseColor != NULL;
    }

    bool hasSpecularColor() {
        return specularColor != NULL;
    }

    bool hasShininessPercent() {
        return shininessPercent != -1;
    }

    bool hasShininessStrengthPercent() {
        return shininessStrengthPercent != -1;
    }

    bool hasTransparencyFalloffPercent() {
        return transparencyFalloffPercent != -1;
    }

    bool hasTransparencyPercent() {
        return transparencyPercent != -1;
    }

    DsColor* getAmbientColor() const {
        return ambientColor;
    }

    void setAmbientColor(DsColor* ambientColor) {
        this->ambientColor = ambientColor;
    }

    DsColor* getDiffuseColor() const {
        return diffuseColor;
    }

    void setDiffuseColor(DsColor* diffuseColor) {
        this->diffuseColor = diffuseColor;
    }

    DsColor* getSpecularColor() const {
        return specularColor;
    }

    void setSpecularColor(DsColor* specularColor) {
        this->specularColor = specularColor;
    }

    //    Image* getTexture() const {
    //        return texture;
    //    }
    //
    //    void setTexture(Image* texture) {
    //        this->texture = texture;
    //    }

    char* getTexturePath() const {
        return texturePath;
    }

    void setTexturePath(char* texturePath) {
        this->texturePath = texturePath;
    }

    //    Image* getTexture2() const {
    //        return texture2;
    //    }
    //
    //    void setTexture2(Image* texture2) {
    //        this->texture2 = texture2;
    //    }

    char* getTexturePath2() const {
        return texturePath2;
    }

    void setTexturePath2(char* texturePath2) {
        this->texturePath2 = texturePath2;
    }

    int getTransparencyFalloffPercent() const {
        return transparencyFalloffPercent;
    }

    void setTransparencyFalloffPercent(int transparencyFalloffPercent) {
        this->transparencyFalloffPercent = transparencyFalloffPercent;
    }

    int getTransparencyPercent() const {
        return transparencyPercent;
    }

    void setTransparencyPercent(int transparencyPercent) {
        this->transparencyPercent = transparencyPercent;
    }

    int getShininessPercent() const {
        return shininessPercent;
    }

    void setShininessPercent(int shininessPercent) {
        this->shininessPercent = shininessPercent;
    }

    int getShininessStrengthPercent() const {
        return shininessStrengthPercent;
    }

    void setShininessStrengthPercent(int shininessStrengthPercent) {
        this->shininessStrengthPercent = shininessStrengthPercent;
    }

    const char* getName() const {
        return name;
    }

    virtual ~Material() {
        if (hasTexture())
            delete texturePath;
        //        if (isTextureLoaded())
        //            delete texture;

        if (hasTexture2())
            delete texturePath2;
        //        if (isTexture2Loaded())
        //            delete texture2;

        if (hasAmbientColor())
            delete ambientColor;
        if (hasDiffuseColor())
            delete diffuseColor;
        if (hasSpecularColor())
            delete specularColor;
    }

    unsigned int getTextureGLid() const {
        return textureGLid;
    }

    void setTextureGLid(unsigned int textureGLid) {
        this->textureGLid = textureGLid;
    }

    bool isTexture2Loaded() const {
        return texture2Loaded;
    }

    void setTexture2Loaded(bool texture2Loaded) {
        this->texture2Loaded = texture2Loaded;
    }

    bool isTextureLoaded() const {
        return textureLoaded;
    }

    void setTextureLoaded(bool textureLoaded) {
        this->textureLoaded = textureLoaded;
    }
};

#endif	/* MATERIAL_H */

