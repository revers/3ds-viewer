/* 
 * File:   ChunkTreeDefinition.h
 * Author: Revers
 *
 * Created on 29 październik 2010, 14:17
 */

#ifndef CHUNKTREEDEFINITION_H
#define	CHUNKTREEDEFINITION_H

#include <string>

const std::string CHUNK_TREE_LINES[] = {
    std::string("0x4D4D : Main chunk"),
    std::string("  0x0002 : 3DS-Version"),
    std::string("  0x3D3D : 3D editor chunk"),
    std::string("    0x0100 : One unit"),
    std::string("    0x1100 : Background bitmap"),
    std::string("    0x1101 : Use background bitmap"),
    std::string("    0x1200 : Background color"),
    std::string("      0x0010 : Rgb (float)"),
    std::string("      0x0013 : Rgb (float) gamma corrected"),
    std::string("    0x1201 : Use background color"),
    std::string("    0x1300 : Gradient colors"),
    std::string("      0x0010 : Rgb (float)"),
    std::string("      0x0013 : Rgb (float) gamma corrected"),
    std::string("    0x1301 : Use gradient"),
    std::string("    0x1400 : Shadow map bias"),
    std::string("    0x1420 : Shadow map size"),
    std::string("    0x1450 : Shadow map sample range"),
    std::string("    0x1460 : Raytrace bias"),
    std::string("    0x1470 : Raytrace on"),
    std::string("    0x2100 : Ambient color"),
    std::string("      0x0010 : Rgb (float)"),
    std::string("      0x0011 : Rgb (byte)"),
    std::string("      0x0012 : Rgb (byte) gamma corrected"),
    std::string("      0x0013 : Rgb (float) gamma corrected"),
    std::string("    0x2200 : Fog"),
    std::string("      0x2210 : fog background"),
    std::string("    0x2201 : Use fog"),
    std::string("    0x2210 : Fog background"),
    std::string("    0x2300 : Distance queue"),
    std::string("      0x2310 : Dim background"),
    std::string("    0x2301 : Use distance queue"),
    std::string("    0x2302 : Layered fog options"),
    std::string("    0x2303 : Use layered fog"),
    std::string("    0x3D3E : Mesh version"),
    std::string("    0x4000 : Object block"),
    std::string("      0x4010 : Object hidden"),
    std::string("      0x4012 : Object doesn't cast"),
    std::string("      0x4013 : Matte object"),
    std::string("      0x4015 : External process on"),
    std::string("      0x4017 : Object doesn't receive shadows"),
    std::string("      0x4100 : Triangular mesh"),
    std::string("        0x4110 : Vertices list"),
    std::string("        0x4120 : Faces description"),
    std::string("          0x4130 : Faces material list"),
    std::string("          0x4150 : Smoothing group list"),
    std::string("        0x4140 : Mapping coordinates list"),
    std::string("        0x4160 : Local coordinate system"),
    std::string("        0x4165 : Object color in editor"),
    std::string("          0x0010 : Rgb (float)"),
    std::string("          0x0011 : Rgb (byte)"),
    std::string("          0x0012 : Rgb (byte) gamma corrected"),
    std::string("          0x0013 : Rgb (float) gamma corrected"),
    std::string("        0x4181 : External process name"),
    std::string("        0x4182 : External process parameters"),
    std::string("      0x4600 : Light"),
    std::string("        0x4610 : Spotlight"),
    std::string("          0x4627 : Spot raytrace"),
    std::string("          0x4630 : Light shadowed"),
    std::string("          0x4641 : Spot shadow map"),
    std::string("          0x4650 : Spot show cone"),
    std::string("          0x4651 : Spot is rectangular"),
    std::string("          0x4652 : Spot overshoot"),
    std::string("          0x4653 : Spot map"),
    std::string("          0x4656 : Spot roll"),
    std::string("          0x4658 : Spot ray trace bias"),
    std::string("        0x4620 : Light off"),
    std::string("        0x4625 : Attenuation on"),
    std::string("        0x4659 : Range start"),
    std::string("        0x465A : Range end"),
    std::string("        0x465B : Multiplier"),
    std::string("      0x4700 : Camera"),
    std::string("    0x7001 : Window settings"),
    std::string("      0x7011 : Window description #2 ..."),
    std::string("      0x7012 : Window description #1 ..."),
    std::string("      0x7020 : Mesh windows ..."),
    std::string("    0xAFFF : Material block"),
    std::string("      0xA000 : Material name"),
    std::string("      0xA010 : Ambient color"),
    std::string("        0x0010 : Rgb (float)"),
    std::string("        0x0011 : Rgb (byte)"),
    std::string("        0x0012 : Rgb (byte) gamma corrected"),
    std::string("        0x0013 : Rgb (float) gamma corrected"),
    std::string("      0xA020 : Diffuse color"),
    std::string("        0x0010 : Rgb (float)"),
    std::string("        0x0011 : Rgb (byte)"),
    std::string("        0x0012 : Rgb (byte) gamma corrected"),
    std::string("        0x0013 : Rgb (float) gamma corrected"),
    std::string("      0xA030 : Specular color"),
    std::string("        0x0010 : Rgb (float)"),
    std::string("        0x0011 : Rgb (byte)"),
    std::string("        0x0012 : Rgb (byte) gamma corrected"),
    std::string("        0x0013 : Rgb (float) gamma corrected"),
    std::string("      0xA040 : Shininess percent"),
    std::string("        0x0030 : percent (int)"),
    std::string("        0x0031 : percent (float)"),
    std::string("      0xA041 : Shininess strength percent"),
    std::string("        0x0030 : percent (int)"),
    std::string("        0x0031 : percent (float)"),
    std::string("      0xA050 : Transparency percent"),
    std::string("        0x0030 : percent (int)"),
    std::string("        0x0031 : percent (float)"),
    std::string("      0xA052 : Transparency falloff percent"),
    std::string("        0x0030 : percent (int)"),
    std::string("        0x0031 : percent (float)"),
    std::string("      0xA053 : Reflection blur percent"),
    std::string("        0x0030 : percent (int)"),
    std::string("        0x0031 : percent (float)"),
    std::string("      0xA081 : 2 sided"),
    std::string("      0xA083 : Add trans"),
    std::string("      0xA084 : Self illum"),
    std::string("      0xA085 : Wire frame on"),
    std::string("      0xA087 : Wire thickness"),
    std::string("      0xA088 : Face map"),
    std::string("      0xA08A : In tranc"),
    std::string("      0xA08C : Soften"),
    std::string("      0xA08E : Wire in units"),
    std::string("      0xA100 : Render type"),
    std::string("      0xA240 : Transparency falloff percent present"),
    std::string("      0xA250 : Reflection blur percent present"),
    std::string("      0xA252 : Bump map present (true percent)"),
    std::string("      0xA200 : Texture map 1"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA33A : Texture map 2"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA210 : Opacity map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA230 : Bump map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA33C : Shininess map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA204 : Specular map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B	  "),
    std::string("      0xA33D : Self illum. map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA220 : Reflection map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA33E : Mask for texture map 1"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA340 : Mask for texture map 2"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA342 : Mask for opacity map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA344 : Mask for bump map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA346 : Mask for shininess map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA348 : Mask for specular map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA34A : Mask for self illum. map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("      0xA34C : Mask for reflection map"),
    std::string("        0xA300 : Mapping filename"),
    std::string("        0xA351 : Mapping parameters"),
    std::string("        0xA353 : Blur percent"),
    std::string("          0x0030 : percent (int)"),
    std::string("          0x0031 : percent (float)"),
    std::string("        0xA354 : V scale"),
    std::string("        0xA356 : U scale"),
    std::string("        0xA358 : U offset"),
    std::string("        0xA35A : V offset"),
    std::string("        0xA35C : Rotation angle"),
    std::string("        0xA360 : RGB Luma/Alpha tint 1"),
    std::string("        0xA362 : RGB Luma/Alpha tint 2"),
    std::string("        0xA364 : RGB tint R"),
    std::string("        0xA366 : RGB tint G"),
    std::string("        0xA368 : RGB tint B"),
    std::string("  0xB000 : Keyframer chunk"),
    std::string("    0xB001 : Ambient light information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB002 : Mesh information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB003 : Camera information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB004 : Camera target information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB005 : Omni light information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB006 : Spot light target information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB007 : Spot light information block"),
    std::string("      0xB010 : Object name, parameters and hierarchy father"),
    std::string("      0xB011 : INSTANCE_NAME "),
    std::string("      0xB012 : PRESCALE"),
    std::string("      0xB013 : Object pivot point"),
    std::string("      0xB014 : Bound Box"),
    std::string("      0xB015 : Object morph angle"),
    std::string("      0xB020 : Position track"),
    std::string("      0xB021 : Rotation track"),
    std::string("      0xB022 : Scale track"),
    std::string("      0xB023 : FOV track"),
    std::string("      0xB024 : Roll track"),
    std::string("      0xB025 : Color track"),
    std::string("      0xB026 : Morph track"),
    std::string("      0xB027 : Hotspot track"),
    std::string("      0xB028 : Falloff track"),
    std::string("      0xB029 : Hide track"),
    std::string("      0xB030 : Hierarchy position"),
    std::string("    0xB008 : Frames (Start and End)"),
    std::string("    0xB009 : KFCURTIME"),
    std::string("    0xB00A : KFHDR"),
};

const int CHUNK_TREE_LINES_LENGTH =
        sizeof (CHUNK_TREE_LINES) / sizeof (std::string);


#endif	/* CHUNKTREEDEFINITION_H */

