/* 
 * File:   Chunk.h
 * Author: Revers
 *
 * Created on 20 październik 2010, 17:56
 */

#ifndef CHUNK_H
#define	CHUNK_H

#include <list>
#include <vector>
//#include <stdexcept>
#include "RevArray.h"
#include "ChunkIDs.h"

using rev::util::Array;
using rev::util::SubArray;
using rev::util::ByteArrayStream;

class Chunk;
class ChunkDesc;

typedef Array<Chunk*> ChunkArray;
typedef SubArray<char> ChunkData;
typedef Array<ChunkDesc*> ChunkDescArray;

const unsigned int CHUNK_HEADER_SIZE = 6;

class Chunk {
    unsigned int id;
    const char* name;
    ChunkData *data;
    Chunk* parent;
    ChunkArray *subChunks;
    unsigned int fileOffset;

    static void printChunkTree(ChunkArray *chunkTree, int level);
    static ChunkArray*
    getSubChunks(ByteArrayStream& stream, ChunkDescArray* subChunkDescs,
            Chunk* parent, int parentChunkOffset = 0);
public:

    Chunk(unsigned short id, const char* name, char* dataBegin, int dataLength) :
    id(id), name(name), parent(NULL), subChunks(NULL) {
        fileOffset = 0;
        data = new ChunkData(dataBegin, dataLength);
    }

    Chunk(Chunk* parent, unsigned short id, const char* name,
            char* dataBegin, int dataLength) :
    id(id), name(name), parent(parent), subChunks(NULL) {
        fileOffset = 0;
        data = new ChunkData(dataBegin, dataLength);
    }

    int getLength() {
        return data->length();
    }

    void setSubChunks(ChunkArray* subChunks) {
        this->subChunks = subChunks;
    }

    ChunkArray* getSubChunks() const {
        return subChunks;
    }

    ChunkData* getData() const {
        return data;
    }

    unsigned int getId() const {
        return id;
    }

    const char* getName() const {
        return name;
    }

    Chunk* getParent() const {
        return parent;
    }

    virtual ~Chunk();

    std::string& toString(std::string& s);

    static void printChunkTree(ChunkArray* chunkTree) {
        printChunkTree(chunkTree, 0);
    }

    unsigned int getFileOffset() const {
        return fileOffset;
    }

    void setFileOffset(unsigned int arrayOffset) {
        this->fileOffset = arrayOffset;
    }

    static ChunkArray*
    getChunkTree(Array<char>* modelData, ChunkDescArray* chunkDescTree);

    static void deleteChunkTree(ChunkArray* chunkTree);
};

//-----------------------------------------------------------------------------------

class ChunkDesc {
    std::string *name;
    std::string *hexId;
    unsigned short id;
    ChunkDescArray *subChunkDescs;

    static void printChunkDescTree(ChunkDescArray *chunkDescTree, int level);

    static ChunkDescArray * getChunkTree(int level, int index);// throw (std::runtime_error);

    static int getElementCount(ChunkDesc &chunk);

public:

    ChunkDesc(std::string* name, std::string* hexId, unsigned short id) :
    name(name), hexId(hexId), id(id) {
        subChunkDescs = NULL;
    }

    virtual ~ChunkDesc();

    static ChunkDescArray* getChunkDescTree() {// throw (std::runtime_error) {
        return getChunkTree(0, 0);
    }

    static void deleteChunkDescTree(ChunkDescArray* chunkDescTree);

    static void printChunkDescTree(ChunkDescArray *chunkDescTree) {
        printChunkDescTree(chunkDescTree, 0);
    }

    ChunkDescArray * getSubChunkDescs() const {
        return subChunkDescs;
    }

    void setSubChunkDescs(ChunkDescArray * subChunkDescs) {
        this->subChunkDescs = subChunkDescs;
    }

    std::string* getHexId() const {
        return hexId;
    }

    const unsigned short getId() const {
        return id;
    }

    std::string* getName() const {
        return name;
    }

    std::string& toString(std::string& s);


};


#endif	/* CHUNK_H */

