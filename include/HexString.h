/* 
 * File:   HexString.h
 * Author: Revers
 *
 * Created on 20 październik 2010, 15:56
 */

#ifndef HEXSTRING_H
#define	HEXSTRING_H

#include <iostream>

class HexString {
    char* buff;
    unsigned long long value;
    int buffSize;

    void toHexStr(char* buff, int bufflen,
            unsigned long long value, bool withSpaces);

public:
    HexString(const HexString& hex);
    HexString(unsigned long long value, bool withSpaces = false);
    HexString(long long value, bool withSpaces = false);
    HexString(unsigned int value, bool withSpaces = false);
    HexString(int value, bool withSpaces = false);
    HexString(unsigned short value, bool withSpaces = false);
    HexString(short value, bool withSpaces = false);
    HexString(unsigned char value, bool withSpaces = false);
    HexString(char value, bool withSpaces = false);

    virtual ~HexString() {
        free(buff);
    }

    int length() const {
        return buffSize - 1;
    }

    unsigned long long getValue() const {
        return value;
    }

    const char* c_str() {
        return buff;
    }

    std::string toString();

    friend std::ostream & operator<<(std::ostream&, HexString&);

    static unsigned long long valueOf(std::string &hexString);

    static unsigned long long valueOf(std::string *hexString) {
        return valueOf(*hexString);
    }
};



#endif	/* HEXSTRING_H */

