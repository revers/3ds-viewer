/* 
 * File:   MeshInfoBlock.h
 * Author: Revers
 *
 * Created on 2 listopad 2010, 04:59
 */

#ifndef MESHINFOBLOCK_H
#define	MESHINFOBLOCK_H

#include "Object3D.h"
#include "RevGraphics.h"

using namespace rev::graph;

class MeshInfoBlock {
private:

    std::string removeNull(Point3f* p) {
        if (p == NULL) {
            return std::string("NULL");
        } else {
            return p->toString();
        }
    }

public:
    Point3f* positionValues;
    Point3f* scaleValues;
    Point3f* pivotValues;
    Point3f* rotationAxisValues;
    float rotationAngle;
    char* name;
    char* instanceName;
    int hierarchyPosition;
    int hierarchyFather;
    Array<MeshInfoBlock*>* subMeshInfos;
    Matrix44f transformMatrix;

    Object3D* object3D;

    MeshInfoBlock() : 
    positionValues(NULL),
    scaleValues(NULL), pivotValues(NULL),
    rotationAxisValues(NULL),
    rotationAngle(-1.0f), name(NULL), instanceName(NULL),
    hierarchyPosition(-1), hierarchyFather(-1),
    subMeshInfos(NULL), transformMatrix(Matrix44f::createIdentityMatrix()),
    object3D(NULL) {

    }

    std::string getFullName() {
        if (name != NULL && instanceName != NULL) {
            return std::string(name) + std::string("/") + std::string(instanceName);
        } else if (name != NULL) {
            return std::string(name);
        } else if (instanceName != NULL) {
            return std::string(instanceName);
        } else {
            return std::string("NULL");
        }
    }

    int getHierarchyFather() const {
        return hierarchyFather;
    }

    void setHierarchyFather(int hierarchyFather) {
        this->hierarchyFather = hierarchyFather;
    }

    int getHierarchyPosition() const {
        return hierarchyPosition;
    }

    void setHierarchyPosition(int hierarchyPosition) {
        this->hierarchyPosition = hierarchyPosition;
    }

    char* getInstanceName() const {
        return instanceName;
    }

    void setInstanceName(char* instanceName) {
        this->instanceName = instanceName;
    }

    char* getName() const {
        return name;
    }

    void setName(char* name) {
        this->name = name;
    }

    Point3f* getPivotValues() const {
        return pivotValues;
    }

    void setPivotValues(Point3f* pivotValues) {
        this->pivotValues = pivotValues;
    }

    Point3f* getPositionValues() const {
        return positionValues;
    }

    void setPositionValues(Point3f* positionValues) {
        this->positionValues = positionValues;
    }

    float getRotationAngle() const {
        return rotationAngle;
    }

    void setRotationAngle(float rotationAngle) {
        this->rotationAngle = rotationAngle;
    }

    Point3f* getRotationAxisValues() const {
        return rotationAxisValues;
    }

    void setRotationAxisValues(Point3f* rotationAxisValues) {
        this->rotationAxisValues = rotationAxisValues;
    }

    Point3f* getScaleValues() const {
        return scaleValues;
    }

    void setScaleValues(Point3f* scaleValues) {
        this->scaleValues = scaleValues;
    }

    Array<MeshInfoBlock*>* getSubMeshInfos() const {
        return subMeshInfos;
    }

    void setSubMeshInfos(Array<MeshInfoBlock*>* subMeshInfos) {
        this->subMeshInfos = subMeshInfos;
    }

    Matrix44f& getTransformMatrix() {
        return transformMatrix;
    }

    void setTransformMatrix(Matrix44f transformMatrix) {
        this->transformMatrix = transformMatrix;
    }

    Object3D* getObject3D() const {
        return object3D;
    }

    void setObject3D(Object3D* object3D) {
        this->object3D = object3D;
    }

    ~MeshInfoBlock() {
        if (scaleValues != NULL) {
            delete scaleValues;
        }

        if (positionValues != NULL) {
            delete positionValues;
        }

        if (pivotValues != NULL) {
            delete pivotValues;
        }

        if (rotationAxisValues != NULL) {
            delete rotationAxisValues;
        }

        if (name != NULL) {
            delete name;
        }

        if (instanceName != NULL) {
            delete instanceName;
        }

        if (subMeshInfos != NULL) {
            delete subMeshInfos;
        }
    }

    std::string toString() {
        std::ostringstream oss;
        oss << getFullName()
                << "; POS: " << removeNull(positionValues)
                << "; PIVOT: " << removeNull(pivotValues)
                << "; ROTATION: " << removeNull(rotationAxisValues)
                << ", angle = " << rotationAngle
                << "; SCALE: " << removeNull(scaleValues);
              //  << "\n";
      //  oss << transformMatrix.toString();

        return oss.str();
    }

    std::string toStringFull() {
        std::ostringstream oss;
        oss << "MeshInfoBlock@" << this << " ";
        oss << getFullName()
                << "; POS: " << removeNull(positionValues)
                << "; PIVOT: " << removeNull(pivotValues)
                << "; ROTATION: " << removeNull(rotationAxisValues)
                << ", angle = " << rotationAngle
                << "; SCALE: " << removeNull(scaleValues);
            //    << "\n";
     //   oss << transformMatrix.toString();

        return oss.str();
    }
};


#endif	/* MESHINFOBLOCK_H */

