/* 
 * File:   RevMath.h
 * Author: Revers
 *
 * Created on 29 październik 2010, 14:45
 */

#ifndef REVMATH_H
#define	REVMATH_H

#include <cmath>



namespace rev {
    namespace math {

        static inline float toDegrees(float radians) {
            //return 180.0f * radians / (float) M_PI;
            return radians * 57.29578f;
        }

        static inline double toDegrees(double radians) {
            // return 180.0 * radians / M_PI;
            return radians * 57.29577951308232;
        }

        static inline float toRadians(float degrees) {
            // return (float) M_PI * degrees / 180.0f;
            return degrees * 0.017453292f;
        }

        static inline double toRadians(double degrees) {
            //return M_PI * degrees / 180.0;
            return degrees * 0.017453292519943295;
        }

        //TODO: dodac metody rotate

    } // end namespace math
} // end namespace rev


#endif	/* REVMATH_H */

