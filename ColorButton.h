/* 
 * File:   ColorButton.h
 * Author: Revers
 *
 * Created on 8 listopad 2010, 01:32
 */

#ifndef COLORBUTTON_H
#define	COLORBUTTON_H
#include <QPushButton>
#include <QColor>

class ColorButton : public QPushButton {
    Q_OBJECT
private:
    QColor color;

public:
    ColorButton(QColor color, QWidget* parent = NULL);
    virtual ~ColorButton();
protected:
    void paintEvent(QPaintEvent* pe);

public:
    QColor getColor() const {
        return color;
    }

    void setColor(QColor color) {
        this->color = color;
    }

    void setColor(float r, float g, float b) {
        color.setRedF(r);
        color.setGreenF(g);
        color.setBlueF(b);
    }
};

#endif	/* COLORBUTTON_H */

