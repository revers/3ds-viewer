/*
 * File:   MeshInfoTreeItem.h
 * Author: Revers
 *
 * Created on 2 listopad 2010, 05:06
 */

#ifndef MESHINFOTREEITEM_H
#define	MESHINFOTREEITEM_H
#include <QtGui>
#include "MeshInfoBlock.h"

class MeshInfoTreeItem : public QObject, public QTreeWidgetItem {
    Q_OBJECT;
private:
    MeshInfoBlock* meshInfo;
    bool selected;

public:

    MeshInfoTreeItem(MeshInfoBlock* meshInfo) :
    QTreeWidgetItem(QStringList(meshInfo->getFullName().c_str())),
    meshInfo(meshInfo) {
        selected = true;
    }

    MeshInfoTreeItem(MeshInfoBlock* meshInfo, QTreeWidget *view) :
    QTreeWidgetItem(view, QStringList(meshInfo->getFullName().c_str())),
    meshInfo(meshInfo) {
        selected = true;
    }

public:

    MeshInfoBlock* getMeshInfo() const {
        return meshInfo;
    }

    void setSelected(bool selected) {
        this->selected = selected;
    }

    bool isSelected() const {
        return selected;
    }
};


#endif	/* MESHINFOTREEITEM_H */

