/*
 * File:   QMeshInfoTree.h
 * Author: Revers
 *
 * Created on 2 listopad 2010, 05:26
 */

#ifndef QMESHINFOTREE_H
#define	QMESHINFOTREE_H
#include <vector>
#include <iostream>
#include <QtGui>
#include "treemodel.h"



using std::vector;
using std::cout;
using std::endl;

class QMeshInfoTree : public QTreeView {
    Q_OBJECT

private:
    Array<MeshInfoBlock*>* meshGraph;
    // Array<TreeItem*>* meshItemList;
    vector<TreeItem*> selectedItems;
    vector<TreeItem*> unselectedItems;
    TreeItem* lastSelectedItem;

    TreeModel* treeModel;
public slots:
    void refreshHierarchy();
public:

    QMeshInfoTree(QWidget* parent = NULL) :
    QTreeView(parent),
    meshGraph(NULL),
    lastSelectedItem(NULL),
    treeModel(NULL) {
        init();
    }

    // QMeshInfoTree(Array<MeshInfoBlock*>* meshGraph, QWidget* parent = NULL);

    vector<TreeItem*>& getSelectedItems() {
        return selectedItems;
    }

public:

    //    void setTree(My3dsLoader* loader) {
    //
    //        TreeModel *model = new TreeModel(loader);
    //        setModel(model);
    //        treeModel = model;
    //    }

    void setTree(const char* title, Array<MeshInfoBlock*>* meshGraph) {

        //emit reset();
        treeModel->loadRows(title, meshGraph);

        this->meshGraph = meshGraph;
        emit reset();

        for (int column = 0; column < treeModel->columnCount(); ++column)
            resizeColumnToContents(column);
        // repaint();
    }

//    vector<TreeItem*>* getSelectedItems() const {
//        return &selectedItems;
//    }

private:

    void init() {
        treeModel = new TreeModel;
        setModel(treeModel);

        QObject::connect(this, SIGNAL(clicked(const QModelIndex &)),
                this, SLOT(myClickAction(const QModelIndex &)));
      
        QObject::connect(this, SIGNAL(expanded(const QModelIndex&)),
                this, SLOT(myExpanded(const QModelIndex&)));
    }

public:
signals:
    // wyrzucany jest kiedy zmieniona zostala liczba zaznaczonych ("ptaszkiem")
    // itemow
    void itemCheckedAction(const vector<TreeItem*>& checkedItems,
            const vector<TreeItem*>& uncheckedItems);
    // wyrzucany jest kiedy zostanie podswietlony nowy item
    void itemSelectedAction(TreeItem& meshInfo);

private slots:
    void myClickAction(const QModelIndex & index);
    //    void myItemCheckedAction(const vector<TreeItem*>& selectedItems);
    //    void myItemSelectedAction(TreeItem& meshInfo);
    void myExpanded(const QModelIndex & index);



};

#endif	/* QMESHINFOTREE_H */

