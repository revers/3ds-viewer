/*
 * File:   QMeshInfoTree.cpp
 * Author: Revers
 *
 * Created on 2 listopad 2010, 05:26
 */

#include "QMeshInfoTree.h"

void QMeshInfoTree::refreshHierarchy() {

    Array<TreeItem*> *items = treeModel->getTreeItemList();
    selectedItems.clear();
    unselectedItems.clear();
    for (unsigned int i = 0; i < items->length(); i++) {
        TreeItem* item = (*items)[i];
        MeshInfoBlock* block = item->getMeshInfo();
        Object3D* o3d = block->getObject3D();
        if (o3d != NULL) {
            if (o3d->isSelected()) {
                item->setChecked(true);
                item->setCheckState(0, Qt::Checked);
                selectedItems.push_back(item);
            } else {
                unselectedItems.push_back(item);
                item->setChecked(false);
                item->setCheckState(0, Qt::Unchecked);
            }
        }
    }

    emit dataChanged(QModelIndex(), QModelIndex());
    emit itemCheckedAction(selectedItems, unselectedItems);
}

void QMeshInfoTree::myClickAction(const QModelIndex & index) {
    QTreeWidgetItem *it = (QTreeWidgetItem*) index.internalPointer();

    Qt::CheckState state = it->checkState(CHECK_COLUMN);
    TreeItem* treeItem = ((TreeItem*) it);
    if (treeItem != lastSelectedItem) {
        lastSelectedItem = treeItem;
        emit itemSelectedAction(*lastSelectedItem);
    }

    bool emitSignal = false;
    if (state == Qt::Checked && treeItem->isChecked() == false) {
        emitSignal = true;
    } else if (state == Qt::Unchecked && treeItem->isChecked() == true) {
        emitSignal = true;
    }

    if (emitSignal == true) {

        selectedItems.clear();
        unselectedItems.clear();
        Array<TreeItem*>* treeItemList = treeModel->getTreeItemList();
        for (unsigned int i = 0; i < treeItemList->length(); i++) {
            TreeItem* item = (*treeItemList)[i];


            Qt::CheckState state = item->checkState(CHECK_COLUMN);
            if (state == Qt::Checked) {
                selectedItems.push_back(item);
                item->setChecked(true);
            } else {
                unselectedItems.push_back(item);
                item->setChecked(false);
            }

            MeshInfoBlock* block = item->getMeshInfo();
            if (block->getObject3D() != NULL) {
                block->getObject3D()->setSelected(item->isChecked());
            }
        }

        emit itemCheckedAction(selectedItems, unselectedItems);
    }
}
//
//void QMeshInfoTree::myItemCheckedAction(const vector<TreeItem*>& selectedItems) {
//    cout << "bleble selItems.length = " << selectedItems.size() << endl;
//}
//
//void QMeshInfoTree::myItemSelectedAction(TreeItem& meshInfo) {
//    cout << "Zaznaczono " << meshInfo.getMeshInfo()->toStringFull() << endl;
//}

void QMeshInfoTree::myExpanded(const QModelIndex& index) {
    for (int column = 0; column < treeModel->columnCount(); ++column)
        resizeColumnToContents(column);
}