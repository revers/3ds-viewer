/****************************************************************************
** Meta object code from reading C++ file 'MainWindow.h'
**
** Created: Sat 5. Jan 09:53:39 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "MainWindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MainWindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      20,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      42,   12,   11,   11, 0x08,
      97,   11,   11,   11, 0x08,
     118,   11,   11,   11, 0x08,
     136,   11,   11,   11, 0x08,
     153,   11,   11,   11, 0x08,
     174,   11,   11,   11, 0x08,
     200,   11,   11,   11, 0x08,
     227,   11,   11,   11, 0x08,
     256,   11,   11,   11, 0x08,
     284,   11,   11,   11, 0x08,
     312,   11,   11,   11, 0x08,
     340,   11,   11,   11, 0x08,
     362,   11,   11,   11, 0x08,
     381,   11,   11,   11, 0x08,
     402,  400,   11,   11, 0x08,
     432,  426,   11,   11, 0x08,
     457,  426,   11,   11, 0x08,
     484,  400,   11,   11, 0x08,
     508,  400,   11,   11, 0x08,
     528,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0selectedItems,unselectedItems\0"
    "itemCheckedAction(vector<TreeItem*>,vector<TreeItem*>)\0"
    "openFileMenuAction()\0aboutMenuAction()\0"
    "exitMenuAction()\0showGridMenuAction()\0"
    "changeBgColorMenuAction()\0"
    "enlightObjectsMenuAction()\0"
    "drawOnlySelectedMenuAction()\0"
    "drawCoordSystemMenuAction()\0"
    "drawLocalCoordsMenuAction()\0"
    "drawObjectFrameMenuAction()\0"
    "ambientColorBAction()\0diffColorBAction()\0"
    "specColorBAction()\0d\0lightPosChanged(double)\0"
    "state\0lightOnStateChanged(int)\0"
    "drawLightStateChanged(int)\0"
    "translateAction(double)\0scaleAction(double)\0"
    "rotOkButtonAction()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->itemCheckedAction((*reinterpret_cast< const vector<TreeItem*>(*)>(_a[1])),(*reinterpret_cast< const vector<TreeItem*>(*)>(_a[2]))); break;
        case 1: _t->openFileMenuAction(); break;
        case 2: _t->aboutMenuAction(); break;
        case 3: _t->exitMenuAction(); break;
        case 4: _t->showGridMenuAction(); break;
        case 5: _t->changeBgColorMenuAction(); break;
        case 6: _t->enlightObjectsMenuAction(); break;
        case 7: _t->drawOnlySelectedMenuAction(); break;
        case 8: _t->drawCoordSystemMenuAction(); break;
        case 9: _t->drawLocalCoordsMenuAction(); break;
        case 10: _t->drawObjectFrameMenuAction(); break;
        case 11: _t->ambientColorBAction(); break;
        case 12: _t->diffColorBAction(); break;
        case 13: _t->specColorBAction(); break;
        case 14: _t->lightPosChanged((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 15: _t->lightOnStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 16: _t->drawLightStateChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 17: _t->translateAction((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 18: _t->scaleAction((*reinterpret_cast< double(*)>(_a[1]))); break;
        case 19: _t->rotOkButtonAction(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 20)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 20;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
