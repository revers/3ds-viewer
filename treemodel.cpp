#include <QtGui>

#include "treeitem.h"
#include "treemodel.h"

using namespace std;

//TreeModel::TreeModel(const QStringList &headers, const QString &data,
//        QObject *parent)
//: QAbstractItemModel(parent) {
//    QVector<QVariant> rootData;
//    foreach(QString header, headers)
//    rootData << header;
//
//    rootItem = new TreeItem(rootData);
//    setupModelData(data.split(QString("\n")), rootItem);
//}

TreeModel::TreeModel(Array<MeshInfoBlock*>* meshGraph, QObject* parent) :
QAbstractItemModel(parent) {
    //    meshItemList = NULL;
    //    loader = NULL;
    //    meshGraph = NULL;
    rootItem = new TreeItem(TREE_COLUMN_NAME);
    //this->loader = loader;
    //this->meshGraph = meshGraph;
    loadRows("", meshGraph);
}

void TreeModel::loadRows(const char* title, Array<MeshInfoBlock*>* meshGraph) {
    removeAllRows();
    if (rootItem != NULL)
        delete rootItem;
    rootItem = new TreeItem(title);
    this->meshGraph = meshGraph;
    //lastSelectedItem = NULL;
    vector<TreeItem*> vect;

    QFlags<Qt::ItemFlag> flagNormal = Qt::ItemIsUserCheckable
            | Qt::ItemIsSelectable | Qt::ItemIsTristate | Qt::ItemIsEnabled;

    for (unsigned int i = 0; i < meshGraph->length(); i++) {
        MeshInfoBlock *meshInfo = (*meshGraph)[i];
        TreeItem* item = new TreeItem(meshInfo, rootItem);
        rootItem->addChild(item);

        vect.push_back(item);

        item->setFlags(flagNormal);
        item->setCheckState(0, Qt::Checked);

        if (meshInfo->getSubMeshInfos() != NULL) {
            Array<TreeItem*>* childs = loadNodes(meshInfo->getSubMeshInfos(), item);
            if (childs != NULL) {
                for (unsigned int i = 0; i < childs->length(); i++) {
                    vect.push_back((*childs)[i]);

                }
                delete childs;
            }
        }
    }

    if (vect.size() != 0) {
        treeItemList = new Array<TreeItem*>(vect.size());
        for (unsigned int i = 0; i < treeItemList->length(); i++) {
            (*treeItemList)[i] = vect[i];
        }
    }
}

Array<TreeItem*>*
TreeModel::loadNodes(Array<MeshInfoBlock*>* meshInfoGraph, TreeItem* parent) {
    QFlags<Qt::ItemFlag> flagNormal = Qt::ItemIsUserCheckable
            | Qt::ItemIsSelectable | Qt::ItemIsTristate | Qt::ItemIsEnabled;

    vector<TreeItem*> vect;

    for (unsigned int i = 0; i < meshInfoGraph->length(); i++) {
        MeshInfoBlock *meshInfo = (*meshInfoGraph)[i];

        TreeItem* item = new TreeItem(meshInfo, parent);
        vect.push_back(item);

        item->setFlags(flagNormal);
        item->setCheckState(0, Qt::Checked);

        parent->addChild(item);

        if (meshInfo->getSubMeshInfos() != NULL) {

            Array<TreeItem*>* childs =
                    loadNodes(meshInfo->getSubMeshInfos(), item);

            if (childs != NULL) {
                for (unsigned int i = 0; i < childs->length(); i++) {
                    vect.push_back(((*childs)[i]));
                }
                delete childs;
            }
        }
    }

    if (vect.size() != 0) {
        Array<TreeItem*>* result = new Array<TreeItem*>(vect.size());
        for (unsigned int i = 0; i < result->length(); i++) {
            (*result)[i] = vect[i];
        }
        return result;
    } else {
        return NULL;
    }
}

TreeModel::~TreeModel() {
    delete rootItem;
}

int TreeModel::columnCount(const QModelIndex & /* parent */) const {
    return rootItem->columnCount();
}

QVariant TreeModel::data(const QModelIndex &index, int role) const {
    if (role == Qt::CheckStateRole) {
        return getItem(index)->checkState(CHECK_COLUMN);
    }

    if (!index.isValid())
        return QVariant();

    if (role != Qt::DisplayRole && role != Qt::EditRole)
        return QVariant();

    TreeItem *item = getItem(index);

    return item->data(index.column());
}

Qt::ItemFlags TreeModel::flags(const QModelIndex &index) const {

    if (!index.isValid())
        return 0;

    return getItem(index)->flags();
    //    Qt::ItemIsUserCheckable | Qt::ItemIsSelectable | Qt::ItemIsTristate
    //            | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

TreeItem *TreeModel::getItem(const QModelIndex &index) const {
    if (index.isValid()) {
        TreeItem *item = static_cast<TreeItem*> (index.internalPointer());
        if (item) return item;
    }
    return rootItem;
}

QVariant TreeModel::headerData(int section, Qt::Orientation orientation,
        int role) const {
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
        return rootItem->data(section);

    return QVariant();
}

QModelIndex TreeModel::index(int row, int column, const QModelIndex &parent) const {
    if (parent.isValid() && parent.column() != 0)
        return QModelIndex();

    TreeItem *parentItem = getItem(parent);

    TreeItem *childItem = parentItem->child(row);
    if (childItem)
        return createIndex(row, column, childItem);
    else
        return QModelIndex();
}

bool TreeModel::insertColumns(int position, int columns, const QModelIndex &parent) {
    bool success;

    beginInsertColumns(parent, position, position + columns - 1);
    success = rootItem->insertColumns(position, columns);
    endInsertColumns();

    return success;
}

bool TreeModel::insertRows(int position, int rows, const QModelIndex &parent) {
    TreeItem *parentItem = getItem(parent);
    bool success;

    beginInsertRows(parent, position, position + rows - 1);
    success = parentItem->insertChildren(position, rows, rootItem->columnCount());
    endInsertRows();

    return success;
}

QModelIndex TreeModel::parent(const QModelIndex &index) const {
    if (!index.isValid())
        return QModelIndex();

    TreeItem *childItem = getItem(index);
    TreeItem *parentItem = childItem->parent();

    if (parentItem == rootItem)
        return QModelIndex();

    return createIndex(parentItem->childNumber(), 0, parentItem);
}

bool TreeModel::removeColumns(int position, int columns, const QModelIndex &parent) {
    bool success;

    beginRemoveColumns(parent, position, position + columns - 1);
    success = rootItem->removeColumns(position, columns);
    endRemoveColumns();

    if (rootItem->columnCount() == 0)
        removeRows(0, rowCount());

    return success;
}

bool TreeModel::removeRows(int position, int rows, const QModelIndex &parent) {
    TreeItem *parentItem = getItem(parent);
    bool success = true;

    beginRemoveRows(parent, position, position + rows - 1);
    success = parentItem->removeChildren(position, rows);
    endRemoveRows();

    return success;

    //    TreeItem* pItem = parent.isValid() ? static_cast<TreeItem*> (parent.internalPointer()) : rootItem;
    //    //QDomNode Node = pItem->node();
    //    beginRemoveRows(parent, position, position + rows - 1);
    //    while (rows--) {
    //        pItem.removeChild(pItem->child(position + count)->node());
    //    }
    //    endRemoveRows();
    //    return true;
}

int TreeModel::rowCount(const QModelIndex &parent) const {
    TreeItem *parentItem = getItem(parent);

    return parentItem->childCount();
}

void TreeModel::setProperCheckState(const QModelIndex &index, Qt::CheckState state) {
    TreeItem *item = static_cast<TreeItem*> (index.internalPointer());
    item->setCheckState(CHECK_COLUMN, state);
    emit dataChanged(index, index);

    QModelIndex subIndex = index.child(0, 0);
    while (subIndex.isValid()) {
        setProperCheckState(subIndex, state);
        subIndex = subIndex.child(0, 0);
    }

    int row = 1;
    subIndex = index.child(row, 0);
    while (subIndex.isValid()) {
        setProperCheckState(subIndex, state);
        subIndex = index.child(++row, 0);
    }
}

TreeModel::SubItemsState TreeModel::getSubItemsState(const QModelIndex &index,
        bool checkCurrent) {
    int checked = 0;
    int unchecked = 0;
    int partially = 0;
    if (checkCurrent) {
        TreeItem *item = static_cast<TreeItem*> (index.internalPointer());
        switch (item->checkState(CHECK_COLUMN)) {
            case Qt::Checked:
                checked++;
                break;
            case Qt::Unchecked:
                unchecked++;
                break;
            case Qt::PartiallyChecked:
                partially++;
                break;
        }
        if (partially > 0) {
            return ALL_MIXED;
        }
    }

    QModelIndex subIndex = index.child(0, 0);

    while (subIndex.isValid()) {
        SubItemsState state = getSubItemsState(subIndex, true);
        if (state == ALL_MIXED)
            return state;
        else if (state == ALL_CHECKED)
            checked++;
        else
            unchecked++;
        subIndex = subIndex.child(0, 0);
    }

    if (checked > 0 && unchecked > 0) {
        return ALL_MIXED;
    }

    int row = 0;
    subIndex = index.child(row, 0);
    while (subIndex.isValid()) {
        SubItemsState state = getSubItemsState(subIndex, true);
        if (state == ALL_MIXED)
            return state;
        else if (state == ALL_CHECKED)
            checked++;
        else
            unchecked++;

        subIndex = index.child(++row, 0);
    }

    if (checked == 0 && unchecked > 0)
        return ALL_UNCHECKED;

    if (checked > 0 && unchecked == 0)
        return ALL_CHECKED;

    return ALL_MIXED;
}

void TreeModel::setProperCheckState(const QModelIndex &index) {
    TreeItem *item = static_cast<TreeItem*> (index.internalPointer());

    Qt::CheckState state;
    if (item->checkState(CHECK_COLUMN) == Qt::Checked) {
        state = Qt::Unchecked;
    } else {
        state = Qt::Checked;
    }

    setProperCheckState(index, state);

    QModelIndex parentIndex = index.parent();

//    while (parentIndex.isValid()) {
//        SubItemsState subItemsState = getSubItemsState(parentIndex);
//
//        TreeItem *parentItem = static_cast<TreeItem*> (parentIndex.internalPointer());
//
//        switch (subItemsState) {
//            case ALL_CHECKED:
//                parentItem->setCheckState(CHECK_COLUMN, Qt::Checked);
//                break;
//
//            case ALL_UNCHECKED:
//                parentItem->setCheckState(CHECK_COLUMN, Qt::Unchecked);
//                break;
//
//            case ALL_MIXED:
//                parentItem->setCheckState(CHECK_COLUMN, Qt::PartiallyChecked);
//                break;
//        }
//        emit dataChanged(parentIndex, parentIndex);
//        parentIndex = parentIndex.parent();
//    }
}

bool TreeModel::setData(const QModelIndex &index, const QVariant &value,
        int role) {

    if (role == Qt::CheckStateRole) {
        setProperCheckState(index);
        return true;
    }

    if (role != Qt::EditRole)
        return false;

    TreeItem *item = getItem(index);
    bool result = item->setData(index.column(), value);

    if (result)
        emit dataChanged(index, index);

    return result;
}

bool TreeModel::setHeaderData(int section, Qt::Orientation orientation,
        const QVariant &value, int role) {
    if (role != Qt::EditRole || orientation != Qt::Horizontal)
        return false;

    bool result = rootItem->setData(section, value);

    if (result)
        emit headerDataChanged(orientation, section, section);

    return result;
}

//void TreeModel::setupModelData(const QStringList &lines, TreeItem *parent) {
//    QList<TreeItem*> parents;
//    QList<int> indentations;
//    parents << parent;
//    indentations << 0;
//
//    int number = 0;
//
//    while (number < lines.count()) {
//        int position = 0;
//        while (position < lines[number].length()) {
//            if (lines[number].mid(position, 1) != " ")
//                break;
//            position++;
//        }
//
//        QString lineData = lines[number].mid(position).trimmed();
//
//        if (!lineData.isEmpty()) {
//
//            QStringList columnStrings = lineData.split("\t", QString::SkipEmptyParts);
//            QVector<QVariant> columnData;
//            for (int column = 0; column < columnStrings.count(); ++column)
//                columnData << columnStrings[column];
//
//            if (position > indentations.last()) {
//
//
//
//                if (parents.last()->childCount() > 0) {
//                    parents << parents.last()->child(parents.last()->childCount() - 1);
//                    indentations << position;
//                }
//            } else {
//                while (position < indentations.last() && parents.count() > 0) {
//                    parents.pop_back();
//                    indentations.pop_back();
//                }
//            }
//
//
//            TreeItem *parent = parents.last();
//            parent->insertChildren(parent->childCount(), 1, rootItem->columnCount());
//            for (int column = 0; column < columnData.size(); ++column)
//                parent->child(parent->childCount() - 1)->setData(column, columnData[column]);
//        }
//
//        number++;
//    }
//}
