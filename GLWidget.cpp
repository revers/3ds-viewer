#include <QtGui/QtGui>
#include <QtOpenGL>
#include "gl/glu.h"

#include <iostream>
#include <math.h>

#include "SOIL.h"
#include "GLWidget.h"
#include "treeitem.h"


using namespace std;
using namespace rev::math;
using namespace rev::graph;


#ifndef GL_MULTISAMPLE
#define GL_MULTISAMPLE  0x809D
#endif

GLWidget::GLWidget(QWidget *parent) :
QGLWidget(QGLFormat(QGL::SampleBuffers), parent),
drawObjectFrame(true),
drawLocalCoordSystem(true),
drawOnlySelected(false),
enlightSelection(true),
wireMode(false),
lightOn(true),
drawLightSource(false),
drawCoordSystem(true),
leftMBPressed(false),
middleMBPressed(false),
rightMBPressed(false),
cameraDistance(210),
//cameraPhiAngle(0),
//cameraThetaAngle(0),
cameraX(0),
cameraY(0),
cameraZ(0),
ArcBall(NULL),
textures(NULL),
loader(NULL) {
    lightSphere = gluNewQuadric();
    selectionColor.set(1.0, 0.0, 0.0, 1.0);
    bgColor = Qt::black;
    firstShow = true;

    lightPosition.set(0, 0, 0, 1.0);

    ambientColor.set(0.5, 0.5, 0.5, 1.0);
    diffuseColor.set(0.8, 0.8, 0.8, 1.0);
    specularColor.a = 1.0;
}

void GLWidget::resetAll() {

    lightPosition.set(0, 0, 0, 1.0);

    leftMBPressed = false;
    middleMBPressed = false;
    rightMBPressed = false;
    cameraDistance = 210;
    //    cameraPhiAngle = 0;
    //  cameraThetaAngle = 0;
    cameraX = 0;
    cameraY = 0;
    cameraZ = 0;
    delete ArcBall;
    initArcBall();
}

void GLWidget::setExtremePoints(Point3f& minPoint, Point3f& maxPoint) {
    extremePoints.a.set(maxPoint.x, maxPoint.y, maxPoint.z);
    extremePoints.b.set(maxPoint.x, maxPoint.y, minPoint.z);
    extremePoints.c.set(minPoint.x, maxPoint.y, minPoint.z);
    extremePoints.d.set(minPoint.x, maxPoint.y, maxPoint.z);

    extremePoints.e.set(maxPoint.x, minPoint.y, maxPoint.z);
    extremePoints.f.set(maxPoint.x, minPoint.y, minPoint.z);
    extremePoints.g.set(minPoint.x, minPoint.y, minPoint.z);
    extremePoints.h.set(minPoint.x, minPoint.y, maxPoint.z);

    extremePoints.central = (extremePoints.a + extremePoints.g) / 2.0f;
}

//void GLWidget::calcLocalAxes() {
//    localXAxis.set(extremePoints.a.x, extremePoints.central.y,
//            extremePoints.central.z);
//
//    localYAxis.set(extremePoints.central.x, extremePoints.a.y,
//            extremePoints.central.z);
//
//    localZAxis.set(extremePoints.central.x, extremePoints.central.y,
//            extremePoints.a.z);
//
//    localXAxis.normalize();
//    localYAxis.normalize();
//    localZAxis.normalize();
//}

My3dsLoader* GLWidget::loadModel(const char* filename) {
    if (loader != NULL) {
        loader->deleteAllCreatedData();
        delete loader;
        loader = NULL;
    }

    if (textures != NULL) {
        for (TextureMapIterator it = textures->begin(); it != textures->end(); it++) {
            glDeleteTextures(1, &(it->second));
        }
        delete textures;
        textures = NULL;
    }
    loader = My3dsLoader::loadModel(filename);
    if (loader == NULL) {
        return NULL;
    }

    if (firstShow == false) {
        glPopAttrib();
        initializeGL();
        resizeGL(width(), height());
        setWireMode(wireMode);
    } else {
        firstShow = false;
    }

    initTextures();
    resetAll();

    Model3D* model = loader->getModel();

    selectedObjects.clear();
    Array<Object3D*> *objs = model->getObjectList();

    for (unsigned int i = 0; i < objs->length(); i++) {
        Object3D* o3d = (*objs)[i];
        if (o3d->isSelected()) {

            selectedObjects.push_back(o3d);
        }
    }

    translation.set(0, 0, 0);
    scale.set(1, 1, 1);
    rotation.set(0, 0, 0, 1);

    selectionChangedAction();

    //origCentral = extremePoints.central;

    lightPosition.set(extremePoints.central.x, extremePoints.central.y,
            -100, 1.0);



    repaint();
    return loader;
}

inline void setMax(Point3f& maxPoint, Point3f& point) {
    if (point.x > maxPoint.x)
        maxPoint.x = point.x;
    if (point.y > maxPoint.y)
        maxPoint.y = point.y;
    if (point.z > maxPoint.z)
        maxPoint.z = point.z;
}

inline void setMin(Point3f& minPoint, Point3f& point) {
    if (point.y < minPoint.y)
        minPoint.y = point.y;
    if (point.z < minPoint.z)
        minPoint.z = point.z;
    if (point.x < minPoint.x)
        minPoint.x = point.x;
}

inline void getExtremePoints(Bounds& bounds, Matrix44f& transMat,
        Point3f& minResult, Point3f& maxResult) {

    Point3f point = transMat * bounds.a;

    minResult = point;
    maxResult = point;

    point = transMat * bounds.b;
    setMin(minResult, point);
    setMax(maxResult, point);

    point = transMat * bounds.c;
    setMin(minResult, point);
    setMax(maxResult, point);

    point = transMat * bounds.d;
    setMin(minResult, point);
    setMax(maxResult, point);

    point = transMat * bounds.e;
    setMin(minResult, point);
    setMax(maxResult, point);

    point = transMat * bounds.f;
    setMin(minResult, point);
    setMax(maxResult, point);

    point = transMat * bounds.g;
    setMin(minResult, point);
    setMax(maxResult, point);

    point = transMat * bounds.h;
    setMin(minResult, point);
    setMax(maxResult, point);
}

void GLWidget::selectionChangedAction() {
    Point3f minPoint;
    Point3f maxPoint;
    bool pointsInited = false;
    for (unsigned int i = 0; i < selectedObjects.size(); i++) {
        Object3D* o3d = selectedObjects[i];
        Bounds& bounds = o3d->getBounds();

        //Matrix44f transforms = o3d->getRotation().toMatrix44();
        //Matrix44f transMat = Matrix44f::createTranslationMatrix(o3d->getTranslation());
        //Matrix44f scaleMat = Matrix44f::createScaleMatrix(o3d->getScale());
        //transforms *= transMat;
        //transforms *= scaleMat;

        Matrix44f transforms = Matrix44f::createTranslationMatrix(o3d->getTranslation());
        Matrix44f rotMat = o3d->getRotation().toMatrix44();
        Matrix44f scaleMat = Matrix44f::createScaleMatrix(o3d->getScale());
        transforms *= rotMat;
        transforms *= scaleMat;

        Point3f minP;
        Point3f maxP;

        getExtremePoints(bounds, transforms, minP, maxP);

        if (pointsInited == false) {
            minPoint = minP;
            maxPoint = maxP;
            pointsInited = true;
            continue;
        }

        if (minP.x < minPoint.x)
            minPoint.x = minP.x;
        if (minP.y < minPoint.y)
            minPoint.y = minP.y;
        if (minP.z < minPoint.z)
            minPoint.z = minP.z;

        if (maxP.x > maxPoint.x)
            maxPoint.x = maxP.x;
        if (maxP.y > maxPoint.y)
            maxPoint.y = maxP.y;
        if (maxP.z > maxPoint.z)
            maxPoint.z = maxP.z;
    }

    translation.set(0, 0, 0);
    rotation.set(0, 0, 0, 1);
    scale.set(1, 1, 1);

    setExtremePoints(minPoint, maxPoint);

    //    extremePoints.central.subtract(translation);
    //    extremePoints.a.subtract(translation);
    //    extremePoints.b.subtract(translation);
    //    extremePoints.c.subtract(translation);
    //    extremePoints.d.subtract(translation);
    //    extremePoints.e.subtract(translation);
    //    extremePoints.f.subtract(translation);
    //    extremePoints.g.subtract(translation);
    //    extremePoints.h.subtract(translation);
    //
    //    Quaternionf q = rotation.createConjugated();
    //    Matrix33f m = q.toMatrix33();
    //    extremePoints.central = m * extremePoints.central;
    //    extremePoints.a = m * extremePoints.a;
    //    extremePoints.b = m * extremePoints.b;
    //    extremePoints.c = m * extremePoints.c;
    //    extremePoints.d = m * extremePoints.d;
    //    extremePoints.e = m * extremePoints.e;
    //    extremePoints.f = m * extremePoints.f;
    //    extremePoints.g = m * extremePoints.g;
    //    extremePoints.h = m * extremePoints.h;
}

QSize GLWidget::minimumSizeHint() const {
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const {
    return QSize(600, 600);
}

void GLWidget::initArcBall() {
    Matrix3fSetIdentity(&LastRot);
    Matrix3fSetIdentity(&ThisRot);

    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            Transform.M[i + 4 * j] = (i == j) ? 1.0f : 0.0f;
    ArcBall = new ArcBallT(640.0f, 480.0f);
}

void GLWidget::initializeGL() {

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glFrontFace(GL_CCW);
    glEnable(GL_MULTISAMPLE);
    glEnable(GL_TEXTURE_2D);
    glShadeModel(GL_SMOOTH);


    processLight();

    initArcBall();
    glPushAttrib(GL_ALL_ATTRIB_BITS);
}

void GLWidget::processCamera() {
    //    glRotatef(cameraPhiAngle, 0, 1, 0);
    //    glRotatef(cameraThetaAngle, cos(toRadians(cameraPhiAngle)),
    //            0, sin(toRadians(cameraPhiAngle)));
    glTranslatef(cameraX, cameraY, cameraZ);

    glTranslatef(0, 0, -cameraDistance);

    glMultMatrixf(Transform.M);

    cameraLocation[0] = -cameraX - Transform.s.XZ*cameraDistance;
    cameraLocation[1] = -cameraY - Transform.s.YZ*cameraDistance;
    cameraLocation[2] = -cameraZ - Transform.s.ZZ*cameraDistance;
}

void GLWidget::drawCoordAxes() {
    GLboolean lightEnabled;
    glGetBooleanv(GL_LIGHTING, &lightEnabled);

    if (lightEnabled) {
        glDisable(GL_LIGHTING);
    }

    float oldColor[3];
    glGetFloatv(GL_COLOR, oldColor);

    float length = 200.0f;
    glBegin(GL_LINES);
    {
        glColor3f(1, 0, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(length, 0, 0);

        glColor3f(0, 1, 0);
        glVertex3f(0, 0, 0);
        glVertex3f(0, length, 0);

        glColor3f(0, 0, 1);
        glVertex3f(0, 0, 0);
        glVertex3f(0, 0, length);
    }
    glEnd();

    glColor3fv(oldColor);

    if (lightEnabled) {
        glEnable(GL_LIGHTING);
    }
}

void GLWidget::drawLocalCoordAxes() {
    GLboolean lightEnabled;
    glGetBooleanv(GL_LIGHTING, &lightEnabled);

    if (lightEnabled) {
        glDisable(GL_LIGHTING);
    }

    float oldColor[3];
    glGetFloatv(GL_COLOR, oldColor);

    float factor = 25.0f;
    Point3f& p = extremePoints.central;
    glBegin(GL_LINES);
    {
        glColor3f(1, 0, 0);
        glVertex3fv(&(p.x));

        Point3f xAxis = p;
        xAxis.x += factor;
        //xAxis.y += localXAxis.y * factor;
        //xAxis.z += localXAxis.z * factor;
        glVertex3fv(&(xAxis.x));

        glColor3f(0, 1, 0);
        glVertex3fv(&(p.x));
        Point3f yAxis = p;
        // yAxis.x += localYAxis.x * factor;
        yAxis.y += factor;
        // yAxis.z += localYAxis.z * factor;
        glVertex3fv(&(yAxis.x));

        glColor3f(0, 0, 1);
        glVertex3fv(&(p.x));
        Point3f zAxis = p;
        // zAxis.x += localZAxis.x * factor;
        // zAxis.y += localZAxis.y * factor;
        zAxis.z += factor;
        glVertex3fv(&(zAxis.x));
    }
    glEnd();

    glColor3fv(oldColor);

    if (lightEnabled) {
        glEnable(GL_LIGHTING);
    }
}

void GLWidget::drawLocalObjectsFrame() {
    if (drawObjectFrame == false && drawLocalCoordSystem == false)
        return;

    glPushMatrix();

    float angle = rotation.getRotationAngle();
    Vector3f axis = rotation.getRotationVector();
    //cout << "translation = " << translation.toString() << endl;



    glTranslatef(translation.x, translation.y, translation.z);
    glRotatef(toDegrees(angle), axis.x, axis.y, axis.z);
    glScalef(scale.x, scale.y, scale.z);



    GLboolean lightEnabled;
    glGetBooleanv(GL_LIGHTING, &lightEnabled);

    if (lightEnabled) {
        glDisable(GL_LIGHTING);
    }
    //glPushAttrib(GL_LIGHTING);
    //  glDisable(GL_LIGHTING);

    float oldColor[3];
    glGetFloatv(GL_COLOR, oldColor);
    glColor3f(0, 1, 0);

    if (drawObjectFrame) {
        glBegin(GL_LINES);
        {
            // gorna sciana:
            glVertex3fv(&(extremePoints.a.x));
            glVertex3fv(&(extremePoints.b.x));

            glVertex3fv(&(extremePoints.b.x));
            glVertex3fv(&(extremePoints.c.x));

            glVertex3fv(&(extremePoints.c.x));
            glVertex3fv(&(extremePoints.d.x));

            glVertex3fv(&(extremePoints.d.x));
            glVertex3fv(&(extremePoints.a.x));

            glVertex3fv(&(extremePoints.e.x));
            glVertex3fv(&(extremePoints.f.x));

            // dolna sciana:
            glVertex3fv(&(extremePoints.f.x));
            glVertex3fv(&(extremePoints.g.x));

            glVertex3fv(&(extremePoints.g.x));
            glVertex3fv(&(extremePoints.h.x));

            glVertex3fv(&(extremePoints.h.x));
            glVertex3fv(&(extremePoints.e.x));

            // 4 pionowe linie laczace podstawy:
            glVertex3fv(&(extremePoints.d.x));
            glVertex3fv(&(extremePoints.h.x));

            glVertex3fv(&(extremePoints.a.x));
            glVertex3fv(&(extremePoints.e.x));

            glVertex3fv(&(extremePoints.c.x));
            glVertex3fv(&(extremePoints.g.x));

            glVertex3fv(&(extremePoints.b.x));
            glVertex3fv(&(extremePoints.f.x));
        }
        glEnd();
    }

    if (drawLocalCoordSystem) {
        drawLocalCoordAxes();
    }

    glColor3fv(oldColor);
    //glPopAttrib();
    if (lightEnabled) {
        glEnable(GL_LIGHTING);
    }

    glPopMatrix();
}

void GLWidget::drawEveryObjectFrame(Object3D* o3d) {

    Bounds& bounds = o3d->getBounds();
    GLboolean lightEnabled;
    glGetBooleanv(GL_LIGHTING, &lightEnabled);

    if (lightEnabled) {
        glDisable(GL_LIGHTING);
    }
    //glPushAttrib(GL_LIGHTING);
    //  glDisable(GL_LIGHTING);

    float oldColor[3];
    glGetFloatv(GL_COLOR, oldColor);
    glColor3f(0, 1, 0);

    glBegin(GL_LINES);
    {
        // gorna sciana:
        glVertex3fv(&(bounds.a.x));
        glVertex3fv(&(bounds.b.x));

        glVertex3fv(&(bounds.b.x));
        glVertex3fv(&(bounds.c.x));

        glVertex3fv(&(bounds.c.x));
        glVertex3fv(&(bounds.d.x));

        glVertex3fv(&(bounds.d.x));
        glVertex3fv(&(bounds.a.x));

        glVertex3fv(&(bounds.e.x));
        glVertex3fv(&(bounds.f.x));

        // dolna sciana:
        glVertex3fv(&(bounds.f.x));
        glVertex3fv(&(bounds.g.x));

        glVertex3fv(&(bounds.g.x));
        glVertex3fv(&(bounds.h.x));

        glVertex3fv(&(bounds.h.x));
        glVertex3fv(&(bounds.e.x));

        // 4 pionowe linie laczace podstawy:
        glVertex3fv(&(bounds.d.x));
        glVertex3fv(&(bounds.h.x));

        glVertex3fv(&(bounds.a.x));
        glVertex3fv(&(bounds.e.x));

        glVertex3fv(&(bounds.c.x));
        glVertex3fv(&(bounds.g.x));

        glVertex3fv(&(bounds.b.x));
        glVertex3fv(&(bounds.f.x));
    }
    glEnd();

    glColor3fv(oldColor);
    //glPopAttrib();
    if (lightEnabled) {
        glEnable(GL_LIGHTING);
    }

}

void GLWidget::drawLight() {
    GLboolean lightEnabled = glIsEnabled(GL_LIGHTING);


    glDisable(GL_LIGHTING);
    glDisable(GL_LIGHT0);


    glPushMatrix();
    float oldColor[3];
    glGetFloatv(GL_COLOR, oldColor);

    if (isWireMode()) {
        glPolygonMode(GL_FRONT, GL_FILL);
    }

    // glEnable(GL_LIGHT1);

    glLoadIdentity();

    //    float shift = 10.0f;
    //    float pos[] = {lightPosition.x + shift, lightPosition.y,
    //        lightPosition.z, 1.0};
    //    glLightfv(GL_LIGHT1, GL_POSITION, pos);
    //    float col[] = {
    //        1.0, 1.0, 1.0, 1.0
    //    };
    //    float ambi[] = {0.5, 0.5, 0.5, 1.0};
    //
    //
    //    glLightfv(GL_LIGHT1, GL_DIFFUSE, col);
    //    //glLightfv(GL_LIGHT1, GL_SPECULAR, col);
    //    glLightfv(GL_LIGHT1, GL_AMBIENT, ambi);

    // glEnable(GL_LIGHT1);
    //glEnable(GL_LIGHTING);


    float oldDiff[4];
    float oldSpec[4];
    float oldAmbi[4];
    glGetMaterialfv(GL_FRONT, GL_DIFFUSE, oldDiff);
    glGetMaterialfv(GL_FRONT, GL_SPECULAR, oldSpec);
    glGetMaterialfv(GL_FRONT, GL_AMBIENT, oldAmbi);

    glColor3f(1, 1, 0);
    float yellow[] = {0.7, 0.7, 0.0, 1.0};

    glMaterialfv(GL_FRONT, GL_DIFFUSE, yellow);
    glMaterialfv(GL_FRONT, GL_SPECULAR, yellow);
    glMaterialfv(GL_FRONT, GL_AMBIENT, yellow);

    glTranslatef(lightPosition.x, lightPosition.y, lightPosition.z);
    gluSphere(lightSphere, 2.0, 50, 50);

    glMaterialfv(GL_FRONT, GL_DIFFUSE, oldDiff);
    glMaterialfv(GL_FRONT, GL_SPECULAR, oldSpec);
    glMaterialfv(GL_FRONT, GL_AMBIENT, oldAmbi);
    glDisable(GL_LIGHTING);


    //glDisable(GL_LIGHT1);


    if (isWireMode()) {
        glPolygonMode(GL_FRONT, GL_LINE);
    }
    glColor3fv(oldColor);
    // glPolygonMode(GL_FRONT, mode);
    glPopMatrix();

    if (lightEnabled) {
        glEnable(GL_LIGHTING);
        glEnable(GL_LIGHT0);
    }
}

void GLWidget::processLight() {
    glPushMatrix();
    glLoadIdentity();

    if (isLightOn() == false) {
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        return;
    } else {
        glEnable(GL_LIGHTING);

    }

    glEnable(GL_LIGHT0);
    //    float pos[] = {cameraLocation[0], cameraLocation[1],
    //        cameraLocation[2] - cameraDistance, 1.0};
    glLightfv(GL_LIGHT0, GL_POSITION, &(lightPosition.x));

    glLightfv(GL_LIGHT0, GL_DIFFUSE, &(diffuseColor.r));
    glLightfv(GL_LIGHT0, GL_SPECULAR, &(specularColor.r));
    glLightfv(GL_LIGHT0, GL_AMBIENT, &(ambientColor.r));


    glPopMatrix();
}

void GLWidget::paintGL() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    qglClearColor(bgColor);
    glLoadIdentity();

    processLight();

    processCamera();

    if (loader == NULL)
        return;

    Model3D* model = loader->getModel();
    Object3DArray* objects = model->getObjectList();
    for (unsigned int i = 0; i < objects->length(); i++) {
        Object3D* o3d = (*objects)[i];

        if (enlightSelection && o3d->isSelected()) {
            glGetMaterialfv(GL_FRONT, GL_AMBIENT, oldAmbiColor);
            glGetMaterialfv(GL_FRONT, GL_DIFFUSE, oldDiffColor);
            glGetMaterialfv(GL_FRONT, GL_SPECULAR, oldSpecColor);

            glMaterialfv(GL_FRONT, GL_AMBIENT, &(selectionColor.r));
            glMaterialfv(GL_FRONT, GL_DIFFUSE, &(selectionColor.r));
            glMaterialfv(GL_FRONT, GL_SPECULAR, &(selectionColor.r));
        }


        if (drawOnlySelected == false) {
            glLoadName(o3d->getPickingId());
            o3d->draw(enlightSelection);
            //  drawObjectFrame(o3d);
        } else if (o3d->isSelected()) {
            glLoadName(o3d->getPickingId());
            o3d->draw(enlightSelection);
            //  drawObjectFrame(o3d);
        }

        if (enlightSelection && o3d->isSelected()) {
            glMaterialfv(GL_FRONT, GL_AMBIENT, oldAmbiColor);
            glMaterialfv(GL_FRONT, GL_DIFFUSE, oldDiffColor);
            glMaterialfv(GL_FRONT, GL_SPECULAR, oldSpecColor);
        }
    }

    if (drawCoordSystem)
        drawCoordAxes();

    if (isDrawLightSource()) {
        drawLight();
    }

    drawLocalObjectsFrame();
    // drawLocalCoordAxes();

}

void GLWidget::resizeGL(int width, int height) {
    //int side = qMin(width, height);
    //glViewport((width - side) / 2, (height - side) / 2, side, side);
    glViewport(0, 0, width, height);

    float ratio = (float) height / (float) width;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluPerspective(60, ratio, 1, 2000);

    //    glMatrixMode(GL_MODELVIEW);
    //    glLoadIdentity();
    //    gluLookAt(x, y, z, x + lx, y + ly, z + lz, 0.0, 0.0, -1.0);
    //glFrustum(-0.1, 0.1, ratio*-0.1, ratio * 0.1, 0.1, 1000.0);
    //glOrtho(-3, 3, ratio*-3 - 100, ratio * 3, 0.3, 100.0); //rzutowanie rownolegle

    ArcBall->setBounds((float) width, (float) height);

    glMatrixMode(GL_MODELVIEW);
}

void GLWidget::mousePressEvent(QMouseEvent *event) {
    switch (event->button()) {

        case Qt::LeftButton:
            leftMBPressed = true;
            MousePt.s.X = event->x();
            MousePt.s.Y = event->y();
            LastRot = ThisRot;
            ArcBall->click(&MousePt);
            //
            if (loader != NULL)
                glSelection(event->x(), event->y());
            break;
        case Qt::RightButton:
            rightMBPressed = true;
            mousePoint = event->pos();
            break;
        case Qt::MiddleButton:
            middleMBPressed = true;
            mousePoint = event->pos();
            break;
        default:
            break;
    }

    // repaint();
}

void GLWidget::mouseReleaseEvent(QMouseEvent * event) {
    switch (event->button()) {

        case Qt::LeftButton:
            leftMBPressed = false;
            break;
        case Qt::RightButton:
            rightMBPressed = false;
            break;
        case Qt::MiddleButton:
            middleMBPressed = false;
            break;
        default:
            break;
    }

}

#define BUFF_SIZE 256

void GLWidget::glSelection(int x, int y) {
    GLuint buff[BUFF_SIZE] = {0};
    GLint hits, view[4];

    glSelectBuffer(BUFF_SIZE, buff);

    glGetIntegerv(GL_VIEWPORT, view);

    glRenderMode(GL_SELECT);

    glInitNames();

    glPushName(-1);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();

    gluPickMatrix(x, height() - y, 1.0, 1.0, view);

    glViewport(0, 0, width(), height());

    float ratio = (float) height() / (float) width();

    gluPerspective(60, ratio, 1, 2000);


    glMatrixMode(GL_MODELVIEW);

    paintGL();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    hits = glRenderMode(GL_RENDER);

    glMatrixMode(GL_MODELVIEW);

    handleHits(hits, buff);

    repaint();
}

void GLWidget::handleHits(GLint hits, GLuint *names) {
    int id = 0;
    GLubyte max = 0;
    for (int i = 0; i < hits; i++) {
        //        printf("Number: %d\n"
        //                "Min Z: %d\n"
        //                "Max Z: %d\n"
        //                "Name on stack: %d\n",
        //                (GLubyte) names[i * 4],
        //                (GLubyte) names[i * 4 + 1],
        //                (GLubyte) names[i * 4 + 2],
        //                (GLubyte) names[i * 4 + 3]
        //                );

        //GLubyte minZ = (GLubyte) names[i * 4 + 1];
        GLubyte maxZ = (GLubyte) names[i * 4 + 2];
        GLubyte name = (GLubyte) names[i * 4 + 3];
        if (max <= maxZ) {
            max = maxZ;
            id = name;
        }
    }

    Array<Object3D*> *objs = loader->getModel()->getObjectList();
    for (unsigned int i = 0; i < objs->length(); i++) {
        Object3D* o3d = (*objs)[i];
        if (o3d->getPickingId() == id) {
            o3d->setSelected(!o3d->isSelected());
            emit refreshHierarchy();
            break;
        }
    }

    printf("\n");
}

void GLWidget::mouseMoveEvent(QMouseEvent *event) {
    QPoint currentMousePoint = event->pos();
    QPoint mouseShift(currentMousePoint.x() - mousePoint.x(),
            currentMousePoint.y() - mousePoint.y());

    if (mouseShift.x() == 0 && mouseShift.y() == 0)
        return;

    if (leftMBPressed) {
        MousePt.s.X = (GLfloat) currentMousePoint.x();
        MousePt.s.Y = (GLfloat) currentMousePoint.y();
        Quat4fT ThisQuat;
        ArcBall->drag(&MousePt, &ThisQuat);
        Matrix3fSetRotationFromQuat4f(&ThisRot, &ThisQuat);
        Matrix3fMulMatrix3f(&ThisRot, &LastRot);
        Matrix4fSetRotationFromMatrix3f(&Transform, &ThisRot);

    } else if (rightMBPressed) {
        const float mouseSensivity = 75.0f;
        cameraX += mouseShift.x() / mouseSensivity;
        cameraY -= mouseShift.y() / mouseSensivity;
    } //else if (middleMBPressed) {
    //   const float mouseSensitivity = 5.0f;
    //        cameraPhiAngle += mouseShift.x() / mouseSensitivity;
    //   float targetChangeTheta = mouseShift.y() / mouseSensitivity;

    //        if (fabs(cameraThetaAngle + targetChangeTheta) < 90)
    //      cameraThetaAngle += targetChangeTheta;
    //  }

    mousePoint = event->pos();
    repaint();
}

void GLWidget::wheelEvent(QWheelEvent* event) {

    const float mouseSensivity = 10.0f;
    int rollPosition = event->delta();
    cameraDistance *= 1 + rollPosition / abs(rollPosition) / mouseSensivity;
    repaint();
}

//--------------------------------------------------------------

GLuint GLWidget::getTexture(Material* mat, const char* texturePath) {

    for (TextureMapIterator it = textures->begin(); it != textures->end(); it++) {
        if (strcmp((it->first), texturePath) == 0) {
            return it->second;
        }
    }

    cout << "Laduje teksture '" << texturePath << "'" << endl;
    GLuint tex_2d = SOIL_load_OGL_texture(
            texturePath,
            SOIL_LOAD_RGB,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
            );

    if (0 == tex_2d) {
        ifstream imgFile(texturePath);
        if (imgFile) {
            printf("ERROR: Nie moge zaladowac tekstury: %s dla materialu: %s"
                    "\n\tSOIL loading error: '%s'\n", texturePath,
                    mat->getName(),
                    SOIL_last_result());
        } else {
            printf("ERROR: Tekstura: %s dla materialu: %s NIE ISTNIEJE!"
                    "\n\tSOIL loading error: '%s'\n", texturePath,
                    mat->getName(),
                    SOIL_last_result());
        }
        imgFile.close();

        return 0;
    }

    (*textures)[mat->getTexturePath()] = tex_2d;
    return tex_2d;
}

void GLWidget::initTextures() {
    textures = new TextureMap();
    cout << "Laduje tekstury..." << endl;

    MaterialArray* materials = loader->getMaterials();
    if (materials == NULL) {
        return;
    }

    for (unsigned int i = 0; i < materials->length(); i++) {
        Material* mat = (*materials)[i];
        if (mat->hasTexture()) {
            GLint texId = getTexture(mat, mat->getTexturePath());
            if (texId != 0) {
                mat->setTextureGLid(texId);
                mat->setTextureLoaded(true);
            }
        }

        if (mat->hasTexture2()) {
            GLint texId = getTexture(mat, mat->getTexturePath2());
            if (texId != 0) {
                if (mat->hasTexture() == false)
                    mat->setTextureGLid(texId);
                mat->setTexture2Loaded(true);
            }
        }
    }

    switch (textures->size()) {
        case 1:
            cout << textures->size() << " tekstura zaladowane..." << endl;
            break;
        case 2:
        case 3:
        case 4:
            cout << textures->size() << " tekstury zaladowane..." << endl;
            break;
        default:
            cout << textures->size() << " tekstur zaladowanych..." << endl;
            break;
    }
}

void updateObject(Object3D* o3d) {
    Point3f onePoint(1, 1, 1);
    Quaternionf& rotQua = o3d->getRotation();
    Point3f& transPoint = o3d->getTranslation();
    Point3f& scalePoint = o3d->getScale();

    float angle = rotQua.getRotationAngle();
    if (transPoint.isZero() && scalePoint == onePoint && angle == 0.0f) {
        return;
    }

    Matrix44f transforms = Matrix44f::createTranslationMatrix(transPoint);
    Matrix44f rotMat = rotQua.toMatrix44();
    Matrix44f scaleMat = Matrix44f::createScaleMatrix(scalePoint);
    transforms *= rotMat;
    transforms *= scaleMat;

    Array<Point3f>* vertices = o3d->getVertexArray();
    for (unsigned int i = 0; i < vertices->length(); i++) {
        Point3f& p = (*vertices)[i];

        p = transforms * p;
    }

    Bounds& bounds = o3d->getBounds();

    bounds.a = transforms * bounds.a;
    bounds.b = transforms * bounds.b;
    bounds.c = transforms * bounds.c;
    bounds.d = transforms * bounds.d;
    bounds.e = transforms * bounds.e;
    bounds.f = transforms * bounds.f;
    bounds.g = transforms * bounds.g;
    bounds.h = transforms * bounds.h;

    rotQua.set(0, 0, 0, 1);
    scalePoint.set(1, 1, 1);
    transPoint.set(0, 0, 0);
}

void GLWidget::treeItemCheckedAction(const vector<TreeItem*>& checkedItems,
        const vector<TreeItem*>& uncheckedItems) {
    selectedObjects.clear();

    if (checkedItems.size() > 0) {

        for (unsigned int i = 0; i < checkedItems.size(); i++) {

            MeshInfoBlock* block = checkedItems[i]->getMeshInfo();
            Object3D* o3d = block->getObject3D();

            if (o3d == NULL)
                continue;

            updateObject(o3d);
            selectedObjects.push_back(o3d);
        }
    }

    if (uncheckedItems.size() > 0) {

        for (unsigned int i = 0; i < uncheckedItems.size(); i++) {
            MeshInfoBlock* block = uncheckedItems[i]->getMeshInfo();
            Object3D* o3d = block->getObject3D();

            if (o3d == NULL)
                continue;

            updateObject(o3d);
        }

    }

    selectionChangedAction();
    repaint();
}

void GLWidget::translateSelected(float x, float y, float z) {
    for (unsigned int i = 0; i < selectedObjects.size(); i++) {
        Point3f& p = selectedObjects[i]->getTranslation();

        //-----------------------------------
        //        Quaternionf q = selectedObjects[i]->getRotation().createConjugated();
        //
        //        Matrix33f m = q.toMatrix33();
        //        Vector3f vect(x, y, z);
        //        vect = m * vect;
        //
        //        p.x += vect.x;
        //        p.y += vect.y;
        //        p.z += vect.z;
        //-----------------------------------

        p.x += x;
        p.y += y;
        p.z += z;
    }

    //    Matrix33f m2 = rotation.createConjugated().toMatrix33();
    //    Vector3f vect2(x, y, z);
    //    vect2 = m2 * vect2;
    //
    //    translation.x += vect2.x;
    //    translation.y += vect2.y;
    //    translation.z += vect2.z;

    translation.x += x;
    translation.y += y;
    translation.z += z;
}

void GLWidget::rotateSelected(float angleDegree, float x, float y, float z) {
    Vector3f vect(x, y, z);
    float angle = toRadians(angleDegree);
    if (vect.isZero())
        return;
    vect.normalize();

    // {
    //        Quaternionf q = rotation.createConjugated();
    //
    //        Matrix33f m = q.toMatrix33();
    //        Vector3f v = m * vect;
    //        Quaternionf qua = Quaternionf::createQuaternion(angle, v);

    Quaternionf qua = Quaternionf::createFromAngleAxis(angle, vect);
    rotation *= qua;
    //  }

    //Quaternionf qua = Quaternionf::createQuaternion(angle, vect);
    for (unsigned int i = 0; i < selectedObjects.size(); i++) {
        Quaternionf& rotQua = selectedObjects[i]->getRotation();

        //-----------------------------------
        //        Quaternionf q = rotQua.createConjugated();
        //
        //        Matrix33f m = q.toMatrix33();
        //        Vector3f v = m * vect;
        //        // v.normalize();
        //
        //        Quaternionf qua = Quaternionf::createQuaternion(angle, v);
        //-----------------------------------
        rotQua *= qua;
    }
}

void GLWidget::scaleSelected(float x, float y, float z) {

    for (unsigned int i = 0; i < selectedObjects.size(); i++) {
        Point3f& p = selectedObjects[i]->getScale();
        p.x *= x;
        p.y *= y;
        p.z *= z;
    }

    scale.x *= x;
    scale.y *= y;
    scale.z *= z;
}

GLWidget::~GLWidget() {
    if (loader != NULL) {
        loader->deleteAllCreatedData();
        delete loader;
        loader = NULL;
    }

    if (textures != NULL) {
        for (TextureMapIterator it = textures->begin(); it != textures->end(); it++) {
            glDeleteTextures(1, &(it->second));
        }
        delete textures;
        textures = NULL;
    }

    gluDeleteQuadric(lightSphere);
}

