/* 
 * File:   MainWindow.h
 * Author: Revers
 *
 * Created on 3 listopad 2010, 22:05
 */

#ifndef MAINWINDOW_H
#define	MAINWINDOW_H

#include <QMainWindow>
#include "ColorButton.h"
#include "My3dsLoader.h"
#include "QMeshInfoTree.h"
#include "GLWidget.h"


QT_BEGIN_NAMESPACE
class QAction;
class QLabel;
class QMenu;
class QTextEdit;
class QTabWidget;
class QWidget;
class QScrollArea;
class QRadioButton;
class QCheckBox;
class QDoubleSpinBox;
class QGroupBox;

QT_END_NAMESPACE

class MainWindow : public QMainWindow {
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

private slots:
    void itemCheckedAction(const vector<TreeItem*>& selectedItems,
            const vector<TreeItem*>& unselectedItems);
    void openFileMenuAction();
    void aboutMenuAction();
    void exitMenuAction();
    void showGridMenuAction();
    void changeBgColorMenuAction();

    void enlightObjectsMenuAction();
    void drawOnlySelectedMenuAction();
    void drawCoordSystemMenuAction();

    void drawLocalCoordsMenuAction();
    void drawObjectFrameMenuAction();

    void ambientColorBAction();
    void diffColorBAction();
    void specColorBAction();

    void lightPosChanged(double d);
    void lightOnStateChanged(int state);
    void drawLightStateChanged(int state);

    void translateAction(double d);
    void scaleAction(double d);

    void rotOkButtonAction();

private:
    void createMenu();
    void initComponents();
    void connectSlots();
    void clearSpinners();

private:
    Point3f oldScalePoint;
    Point3f oldTransPoint;

private:
    bool canChangeTrans;
    bool canChangeRot;
    bool canChangeScale;

    bool canChangePosition;
    QWidget *mainWidget;

    My3dsLoader* loader;
    GLWidget* glWidget;

private:
    QMenu* fileMenu;
    QMenu* viewMenu;
    QMenu* helpMenu;
    QAction* openFileAction;
    QAction* exitAction;
    QAction* aboutAction;
    QAction* showGridAction;
    QAction* changeBgColorAction;
    QAction* drawCoordSystemAction;

    QAction* drawLocalCoordsAction;
    QAction* drawObjectFrameAction;

    QAction* enlightObjectsAction;
    QAction* drawOnlySelectedAction;

private:
    QScrollArea* meshTreeScroll;
    QMeshInfoTree* meshTree;
    QTabWidget* toolTabWidget;
    QWidget* modelTab;
    QWidget* lightTab;

private:
    QGroupBox* transPanel;
    QGroupBox* rotPanel;
    QGroupBox* scalePanel;

private:
    QCheckBox* transAllCB;
    QLabel* transXL;
    QLabel* transYL;
    QLabel* transZL;
    QDoubleSpinBox* transXSpin;
    QDoubleSpinBox* transYSpin;
    QDoubleSpinBox* transZSpin;

private:
    QLabel* angleL;
    QLabel* rotXL;
    QLabel* rotYL;
    QLabel* rotZL;
    QDoubleSpinBox* angleSpin;
    QPushButton* rotButton;
    QDoubleSpinBox* rotXSpin;
    QDoubleSpinBox* rotYSpin;
    QDoubleSpinBox* rotZSpin;


private:
    QCheckBox* scaleAllCB;
    QLabel* scaleXL;
    QLabel* scaleYL;
    QLabel* scaleZL;
    QDoubleSpinBox* scaleXSpin;
    QDoubleSpinBox* scaleYSpin;
    QDoubleSpinBox* scaleZSpin;

private:
    // QWidget* lightOptionPanel;
    QCheckBox* lightOnCB;
    QCheckBox* drawLightSourceCB;
    QGroupBox* ambientPanel;
    QLabel* ambientColorL;
    ColorButton* ambientColorB;

private:
    QGroupBox* diffPanel;
    QLabel* diffColorL;
    ColorButton* diffColorB;

    QGroupBox* specPanel;
    QLabel* specColorL;
    ColorButton* specColorB;

    QGroupBox* lightPosPanel;
    QLabel* lightPosXL;
    QLabel* lightPosYL;
    QLabel* lightPosZL;

    QDoubleSpinBox* lightPosXSpin;
    QDoubleSpinBox* lightPosYSpin;
    QDoubleSpinBox* lightPosZSpin;
};

#endif	/* MAINWINDOW_H */

